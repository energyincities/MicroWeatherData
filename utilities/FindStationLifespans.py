try:
	import MySQLdb
except ImportError as e:
	print("You dont have MySQLdb installed. To isntall, please following these steps:\n")
	print("""Installation Instructions:
	+1.1  (for Ubuntu) From terminal run "sudo apt-get install
                   python-pip python-dev libmysqlclient-dev".

	1.2  (for Fedora)  From terminal run "sudo dnf install
                   python python-devel mysql-devel redhat-rpm-config gcc".

	1.3  (for Mac)     From termincal run "brew install mysql-connector-c".
                   Should that fail run "brew install mysql".

	1.4  (for Windows) Go to https://packaging.python.org/tutorials/installing-packages/
			   and follow the install instructions. The package name is "mysqlclient".
			   do NOT follow to step 2.

	2.  From terminal run "pip install mysqlclient".
	""")
	exit()

from datetime import datetime
import calendar
import time
import argparse


def main(dbpass: str):

	#: Create a connection to the SQL database
	print("Connecting to SQL database.")
	db = MySQLdb.connect("Morze.seos.uvic.ca", "evins", dbpass, "meteo")

	#: start a db cursor.
	print("Creating a database cursor.")
	cur = db.cursor()

	#: Create an execute a query to grab the ids and names of all stations in the database.
	query = "SELECT id, name FROM station"
	cur.execute(query)
	raw = cur.fetchall()
	#: Store the grabbed ids and names into a sorted (by id) list.
	station_ids = sorted([(id[0], id[1]) for id in raw])

	#: Create a list to store the ids of the sensors found for each station in the
	#: MySQL databse.
	sensor_ids = []

	#: For every id that we previously found, get all the ids of all the sensors
	#: on the station. Store the sensors ids into a list where the index of the
	#: list matches the id of the station.
	for station_id in station_ids:
		#: Query to select the sensors' ids
		query = "SELECT id FROM sensor WHERE stationid = {}".format(station_id[0])
		cur.execute(query)
		raw = cur.fetchall()

		#: Append a list containing the ids for the sensors on this
		#: station.
		sensor_ids.append([item for id in raw for item in id])

	#: Cur year keeps track of the earliest year that data is stored in the mysql
	#: database.
	cur_year = 2002
	#: end_year keeps track of the current year (ie the last year that data
	#: will exist in the database.)
	end_year = datetime.fromtimestamp(time.time()).year

	#: A list containing the 0 padddd month strings.
	month_list = ["01", "02", "03", "04", "05", "06",
		      "07", "08", "09", "10", "11", "12"]

	#: Create a list to contain the timestamps
	timestamps = []
	for station_index, station in enumerate(sensor_ids):
		print("Processing station number {}".format(station_index))
		sensor_timestamps = []
		for sensor in station:
			print("\tProcessing sensor number {}".format(sensor))
			sensor_times = [None, None]
			while cur_year <= end_year:
				print("\t\tProcessing for {}".format(cur_year))
				if cur_year < 2006:
					query = "SELECT timekey FROM sdata_{} WHERE sensorid = {}"\
						" ORDER BY timekey LIMIT 1".format(cur_year, sensor)

					cur.execute(query)
					timestamp = cur.fetchall()
					if timestamp is not ():
						if sensor_times[0] is None:
							sensor_times[0] = timestamp[0][0]
							sensor_times[1] = timestamp[-1][0]
						else:
							sensor_times[1] = timestamp[-1][0]
				else:
					for month in month_list:
						print("\t\t\tProcessing for {}".format(calendar.month_abbr[int(month)]))
						query = "SELECT timekey FROM sdata_{}_{} WHERE sensorid = {}"\
							" ORDER BY timekey DESC LIMIT 1"\
							.format(cur_year, month, sensor)
						cur.execute(query)
						timestamp = cur.fetchall()
						if timestamp is not ():
							if sensor_times[0] is None:
								sensor_times[0] = timestamp[0][0]
								sensor_times[1] = timestamp[-1][0]
							else:
								sensor_times[1] = timestamp[-1][0]
				cur_year += 1

			cur_year = 2002
			sensor_timestamps.append(sensor_times)
		timestamps.append(sensor_timestamps)

	print("Generating table of stations and dates.")

	file_string = ""
	csv_string = ""
	for index, station in enumerate(station_ids):

		if len(timestamps[index]) >= 2:
			if None not in timestamps[index][0] and None not in timestamps[index][1]:
				first_timestamp = min(
							datetime.fromtimestamp(float(timestamps[index][0][0])),
							datetime.fromtimestamp(float(timestamps[index][1][0]))
						     )

				max_unixtime_stamp = max(
							float(timestamps[index][0][1]),
							float(timestamps[index][1][1])
						    )

				if max_unixtime_stamp >= float(time.time()) - 86400:
					last_timestamp = "Current"
				else:
					last_timestamp = datetime.fromtimestamp(max_unixtime_stamp)

				file_string += "{} (id: {}) weather station:\n\tFirst log: {}\n\tLast log: {}\n\n"\
						.format(station[1], station[0], first_timestamp, last_timestamp)
				csv_string += "{}, {}, {}, {},\n".format(station[1], station[0], first_timestamp, last_timestamp)

			elif None not in timestamps[index][0]:
				first_timestamp = datetime.fromtimestamp(float(timestamps[index][0][0]))
				max_unixtime_stamp = float(timestamps[index][0][1])

				if max_unixtime_stamp >= float(time.time()) - 86400:
					last_timestamp = "Current"
				else:
					last_timestamp = datetime.fromtimestamp(max_unixtime_stamp)

				file_string += "{} (id: {}) weather station:\n\tFirst log: {}\n\tLast log: {}\n\n"\
						.format(station[1], station[0], first_timestamp, last_timestamp)
				csv_string += "{}, {}, {}, {},\n".format(station[1], station[0], first_timestamp, last_timestamp)

			elif None not in timestamps[index][1]:
				first_timestamp = datetime.fromtimestamp(float(timestamps[index][1][0]))
				max_unixtime_stamp = float(timestamps[index][1][1])

				if max_unixtime_stamp >= float(time.time()) - 86400:
					last_timestamp = "Current"
				else:
					last_timestamp = datetime.fromtimestamp(max_unixtime_stamp)

				file_string += "{} (id: {}) weather station:\n\tFirst log: {}\n\tLast log: {}\n\n"\
						.format(station[1], station[0], first_timestamp, last_timestamp)
				csv_string += "{}, {}, {}, {},\n".format(station[1], station[0], first_timestamp, last_timestamp)

			else:
				file_string += "{} (id: {}) weather station:\n\tNo data exists.\n\n"\
						.format(station[1], station[0])
				csv_string += "{}, {},-1, -1,\n".format(station[1], station[0])

		elif len(timestamps[index]) == 1:
			if None not in timestamps[index][0]:
				first_timestamp = datetime.fromtimestamp(float(timestamps[index][0][0]))
				max_unixtime_stamp = float(timestamps[index][0][1])

				if max_unixtime_stamp >= float(time.time()) - 86400:
					last_timestamp = "Current"
				else:
					last_timestamp = datetime.fromtimestamp(max_unixtime_stamp)

				file_string += "{} (id: {}) weather station:\n\tFirst log: {}\n\tLast log: {}\n\n"\
						.format(station[1], station[0], first_timestamp, last_timestamp)
				csv_string += "{}, {}, {}, {},\n".format(station[1], station[0], first_timestamp, last_timestamp)
			else:
				file_string += "{} (id: {}) weather station:\n\tNo data exists.\n\n"\
						.format(station[1], station[0])
				csv_string += "{}, {}, -1, -1,\n".format(station[1], station[0])
		else:
			file_string += "{} (id: {}) weather station:\n\tNo data exists.\n\n"\
					.format(station[1], station[0])
			csv_string += "{}, {}, -1, -1,\n".format(station[1], station[0])

	file_output = open("Weather_station_dates.dat", 'w') #: Open the epw file to be written into.
	csv_output = open("Weather_station_dates.csv", 'w')
	file_output.write(file_string) #: Write the generated string to a csv file.
	csv_output.write(csv_string)
	file_output.close() #: Close the epw file.
	csv_output.close()


if __name__ == "__main__":

	parser = argparse.ArgumentParser(description="Command line utility to pull weather data from an sql"\
						     "database.")

	parser.add_argument('-p', '--password', type=str,
			    help='The password for the MySQLDatabse. If none is provided, then it will be read from mysqlpassword.txt'

	args = parser.parse_args()
	if args.password is None:
		print("No password provided, reading from mysqlpassword.txt")
		with open("mysqlpassword.txt") as passfile:
			args.password = passfile.readline()

	main(args.password)
