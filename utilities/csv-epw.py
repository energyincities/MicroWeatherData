import csv
from MySQLUtilities import *
from datetime import datetime
import argparse

""" TODO:
	Finish documentation.
"""

def csv_to_epw(filenames: list):

	#: Setup initial values for use in the converter.
	station = None
	year = None
	epw_string = ""
	stored_epws = []
	prev_date = None

	#: Convert every file in filenames from csv to epw
	for filename in filenames:

		#: Create a csv reader to get the information out of the passed csv files.
		reader = csv.reader(open(filename, newline=""), delimiter = ',', quotechar='|')

		#: For every row in the csv file, generate a row in the proper formatting for
		#: an epw file.
		for raw_row in reader:
			row = [] #: Used to hold variable in their proper types.

			#: Ignore any and all header rows.
			if raw_row[0] == 'Station id':
				continue

			#: For every entry in the current row of the current csv file, append
			#: Either a float of the value found, or None for an empty cell.
			for item in raw_row:
				#: If this item is not an empty string, append a float
				#: of the value found to row.
				if item != '':
					row.append(float(item))
				#: Otherwise if the item is an empty string, append None.
				else:
					row.append(None)

			#: If this is the first loop, then set station to the current station being
			#: looked at. Set year to the year found in that data row, and set the previous date
			#: one one hour preceeding the hour found in this row of data.
			if station is None:
				station = int(row[0])
				year = int(row[1])
				#: Create a datetime object to one hour earier than the time of the current
				#: row.
				prev_date = datetime(int(row[1]), int(row[2]), int(row[3]), int(row[4]) - 1)

			#: If station != row[0], then we are onto a new station, and as such we should write
			#: all found data to a file and move on to this row of data.
			if station != row[0]:

				#: Create a connection to the SQL database
				cnx = get_db_connection("Morze.seos.uvic.ca", "evins", dbpass, "meteo")
				#: Get the name of the station that has the id of the station
				#: that was just being looked at (the value of station)
				station_name = get_station_name(cnx, stationid=station)
				#: Get that station's coordinates.
				station_coords = get_station_coordinates(cnx, stationid=station)
				#: Close the connection as we are now done with it.
				close_connection(cnx)

				#: Create a datetime object equal to an hour before the row currently being
				#: looked at.
				current_date = datetime(int(row[1]), int(row[2]), int(row[3]), int(row[4]) - 1)
				#: Append the information gathered on the current epw along with
				#: the last date that data was gathered for that epw, and current_date
				#: to so that we can later generate missing data.
				stored_epws.append([prev_date, current_date, epw_string])

				#: Set previous date to current date to keep track of it so that we can
				#: reset current_date to the current date.
				prev_date = current_date
				epw_string = "" #: Empty the epw string

				#: Generate a string with information on the current station to write
				#: as one of the header rows for the epw file.
				station_info = "LOCATION,{},BC,CAN,WYEC2-B-24297,717990,{},{},-8.0,{},"\
					.format(
						station_name,
						round(station_coords[0], 2),
						round(station_coords[1], 2),
						station_coords[2]
					)

				#: Generate the epw file's header
				header = station_info + "\nDESIGN CONDITIONS,0,\nTYPICAL/EXTREME PERIODS,0,\nGROUND TEMPERATURES,0,\nHOLIDAYS/DAYLIGHT SAVING,No,0,0,0,\nCOMMENTS 1,,\nCOMMENTS 2,,\nDATA PERIODS,0,1,\n"

				#: For every epw file that was stored in epw_files (this is incase of
				#: multiple years worth of data.)
				for epw in stored_epws:

					#: Generate the filename for this epw based off the date of
					#: its first data, the date of itslast data, and the station's name
					out_filename = "weather_data_for_{}-{}_at_{}.epw"\
						   .format(
							epw[0].strftime("%b_%Y"),
							epw[1].strftime("%b_%Y"),
							station_name
							)

					#: Open a file with this name in write mode, and
					#: write the epw file information to it.
					epw_file = open(out_filename, 'w')
					epw_file.write(header + epw[2])
					epw_file.close()

				#: Reset the value of station so that we can tell when we've moved onto
				#: the next station again.
				station = int(row[0])

			#: If year does not equal row[1] then we have moved onto a new year worth of data.
			#: Because of EnergyPlus's requirement of only 1 year of data per epw, we have to
			#: keep track of this.
			if year != row[1]:

				#: Create a datetime object equal to an hour before the row currently being
				#: looked at.
				current_date = datetime(int(row[1]), int(row[2]), int(row[3]), int(row[4]) - 1)
				#: Append the information gathered on the current epw along with
				#: the last date that data was gathered for that epw, and current_date
				#: to so that we can later generate missing data.
				stored_epws.append([prev_date, current_date, epw_string])

				#: Reset the epw string to write the next year's worth of data.
				epw_string = ""
				#: Store the current date into prev_date so that we can update current_date.
				prev_date = current_date
				#: Set year to the current year so that we can tell when we've moved
				#: onto the next year again.
				year = int(row[1])

			#: Otherwise write the data we get from this hour to epw_string.
			epw_string += get_hour_data(row[1:-2])[0] + '\n'

		#: Create a connection to the SQL database
		cnx = get_db_connection("Morze.seos.uvic.ca", "evins", dbpass, "meteo")
		#: Get the name of the station that has the id of the station
		#: that was just being looked at (the value of station)
		station_name = get_station_name(cnx, stationid=station)
		#: Get that station's coordinates.
		station_coords = get_station_coordinates(cnx, stationid=station)
		#: Close the connection as we are now done with it.
		close_connection(cnx)

		#: Create a datetime object equal to an hour before the row currently being
		#: looked at.
		current_date = datetime(int(row[1]), int(row[2]), int(row[3]), int(row[4]) - 1)
		#: Append the information gathered on the current epw along with
		#: the last date that data was gathered for that epw, and current_date
		#: to so that we can later generate missing data.
		stored_epws.append([prev_date, current_date, epw_string])

		#: Set previous date to current date to keep track of it so that we can
		#: reset current_date to the current date.
		prev_date = current_date
		epw_string = "" #: Empty the epw string

		#: Generate a string with information on the current station to write
		#: as one of the header rows for the epw file.
		station_info = "LOCATION,{},BC,CAN,WYEC2-B-24297,717990,{},{},-8.0,{},"\
			.format(
				station_name,
				round(station_coords[0], 2),
				round(station_coords[1], 2),
				station_coords[2]
			)

		#: Generate the epw file's header
		header = station_info + "\nDESIGN CONDITIONS,0,\nTYPICAL/EXTREME PERIODS,0,\nGROUND TEMPERATURES,0,\nHOLIDAYS/DAYLIGHT SAVING,No,0,0,0,\nCOMMENTS 1,,\nCOMMENTS 2,,\nDATA PERIODS,0,1,\n"

		#: For every epw file that was stored in epw_files (this is incase of
		#: multiple years worth of data.)
		for epw in stored_epws:

			#: Generate the filename for this epw based off the date of
			#: its first data, the date of itslast data, and the station's name
			out_filename = "weather_data_for_{}-{}_at_{}.epw"\
				.format(
					epw[0].strftime("%b_%Y"),
					epw[1].strftime("%b_%Y"),
					station_name
					)

			#: Open a file with this name in write mode, and
			#: write the epw file information to it.
			epw_file = open(out_filename, 'w')
			epw_file.write(header + epw[2])
			epw_file.close()

def get_hour_data(hour_data) -> (str, str):
	""" get_hour_data(list):

	This method takes in the list created by main, and
	creates a string following the csv formatting required
	by Energy Plus.

	Args:
		hour_data(list): A list containing all of the information for
				 one hour's worth of data. For any field in the
				 list that is set to None, the returned value in
				 the string corresponding to that None is defined
				 by the missing_values dictionary below.

	Returns:
		(str, str):	A line in the epw file being generated by the main program that
		   		corresponds to the passed list hour_data. for an epq file, If a
				None value is found in hour_data, then a value from the
				missing_values dictionary is assigned to the string based off the
				index of the found None (eg: if a None is found at index 8,
				then "999" will be added to the string). For a csv however, if
				a None is encountered then an empty field is returned.
				NOTE:
					return[0] is the epw line.
					return[1] is the csv line.
	"""

	#: A dictionary to containing the "missing value" values for
	#: each given entry's position in the csv file. For example
	#: a key value pair of -1: "none" would mean that if there is
	#: no data for position -1, then we should enter the word none.
	#: Please note,
	#: 	The values in this dictionary correspond to the format
	#:	found at:
	#: 	https://bigladdersoftware.com/epx/docs/8-8/auxiliary-programs/energyplus-weather-file-epw-data-dictionary.html
	missing_values = {

		#: Note positions 0 to 5 are required positions and are
		#: filled automatically by the program.
		5: "?9?9?9?9E0?9?9?9*9*9?9*9?9*_?9?9*9*9*9*_*9*9*9*9*9", #: References and data flags
		6: "99.9",	 #: Field: Dry bulb temprature.
		7: "99.9",	 #: Field: Dew point temprature.
		8: "999",	 #: Field: Relative humidity.
		9: "101325",	 #: Field: Atmospheric station pressure.
		10: "9999",	 #: Field: Extraterrestrial horizontal radiation.
		11: "9999",	 #: Field: Extraterrestrial direct normal radiation.
		12: "9999",	 #: Field: Infrared radiation intensity.
		13: "9999",	 #: Field: global horizontal radiation.
		14: "9999",	 #: Field: Direct normal radiation.
		15: "9999",	 #: Field: Diffuse horizontal radiation.
		16: "999900",	 #: Field: Global horizontal illuminance.
		17: "999900",	 #: Field: Direct normal illuminance.
		18: "999900",	 #: Field: Diffuse horizontal illuminance.
		19: "9999",	 #: Field: Zenith luminance.
		20: "999",	 #: Field: Wind direction.
		21: "999",	 #: Field: Wind speed.
		22: "5",	 #: Field: Total sky cover.
		23: "5",	 #: Field: Opaque sky cover.
		24: "777.7",	 #: Field: Visibility.
		25: "77777",	 #: Field: Ceiling height.
		26: "9",	 #: Field: Present weather observations.
		27: "", 	 #: Field: Weather codes.
		28: "0",	 #: Field: Precipitable water.
		29: "0.0000",	 #: Field: Aerosol optical depts.
		30: "0",	 #: Field: Snow depth.
		31: "88",	 #: Field: Days since last snowfall.
		32: "999.000",	 #: Field: Albedo.
		33: "999.0",	 #: Field: Liquid precipitation depth
		34: "99.0" 	 #: Field: Liquid precipitation quantity
	}

	#: EnergyPlus enforces strict rounding rules apply to values it processes.
	#: Because more or less data than could be read from sensors that is at
	#: the time of writing this code, a full table will be created to ensure
	#: all data gets correct rounding without neededing for a later user to
	#: pre-process the data.
	rounding_correction = {
		6: 1,
		7: 1,
		8: 0,
		9: 0,
		10: 0,
		11: 0,
		12: 0,
		13: 0,
		14: 0,
		15: 0,
		16: 0,
		17: 0,
		18: 0,
		19: 0,
		20: 0,
		21: 1,
		22: 0,
		23: 0,
		24: 1,
		25: 0,
		26: 0,
		27: 0,
		28: 0,
		29: 4,
		30: 0,
		31: 0,
		32: 3,
		33: 1,
		34: 1

	}

	epw_builder = "" #: A string used to build a line of the epw file.
	csv_builder = "" #: A string used to build a line of the csv file.

	#: Loop through the passed data.
	for index, data in enumerate(hour_data):

		#: If this item is None then set it to a missing value
		#: based off of the index of the item.
		if data is None:

			#: Append the value corresponding to a missing value
			#: for that index in the csv.
			epw_builder += missing_values[index]

		#: Otherwise append the data found.
		else:
			if index >=6:
				z_format = ".{}f".format(rounding_correction[index])
				value = format(round(data, rounding_correction[index]), z_format)
			else:
				value = str(int(data))

			epw_builder += value
			if index != 5:
				csv_builder += value
			else:
				csv_builder += ","

		#: If this is not the final item, append a comma.
		if index != 34:
			epw_builder += ","
			csv_builder += ","

	return (epw_builder, csv_builder) #: Return the built string.

if __name__ == "__main__":

	#: Create the argument parser for this file.
	parser = argparse.ArgumentParser(description='A command line utility to convert csv files to epw files.')

	#: Add an argument to get the location(s) of the file(s) to convert from csv to epw.
	parser.add_argument('Filenames', nargs='+', type=str, help='The names of the files to convert.')

	#: Parse the arguments
	args = parser.parse_args()

	#: Convert the passed files.
	csv_to_epw(args.Filenames)

