import matplotlib.pyplot as plt
import os
import csv
import MySQLUtilities
from collections import OrderedDict

def main():

	data = {}
	useable_stations = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 38, 39, 40, 41, 42, 43, 55, 56, 57, 58, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 87, 90, 98, 99, 100, 101, 104, 107, 109, 115, 116, 125, 128, 131, 134, 135, 142, 143, 144, 159, 162, 163, 164, 169, 170, 174, 175, 176, 181, 187, 188, 197, 198, 199, 200, 214, 215, 217, 227, 228, 229, 230, 231, 232]


	for path in [os.path.abspath("weather_files/" + f) for f in os.listdir("weather_files/") if f[-4:] == ".epw"]:
		print("Processing ", path)
		with open(path, 'r') as f:
			reader = csv.reader(f)
			raw_data = list(reader)[8:]
			station_id = MySQLUtilities.get_station_id(path[path.find("at_") + 3: path.find(".epw")])

		if "Station: " + str(station_id) not in data.keys() and station_id in useable_stations:
			data["Station: " + str(station_id)] = []
		if len(raw_data) >= 8760 and station_id in useable_stations:
			data["Station: " + str(station_id)].append(raw_data[0][0])

	year = OrderedDict({})
	for i in range(11):
		for station_id, years in data.items():
			if str(2006 + i) in years:
				if str(2006 + i) not in year.keys():
					year[str(2006 + i)] = 0
				year[str(2006 + i)] += 1

	flipped = {}
	for key, value in data.items():
		for year in value:
			if year != '2005' and year != '2004':
				if year not in flipped.keys():
					flipped[year] = [key]
				else:
					flipped[year].append(key)

	print(flipped.keys())

#	print(flipped)

	stations = ["Station: " + str(station) for station in useable_stations]
	for key, value in flipped.items():
		if len(value) > 10:
			stations = list(set(stations).intersection(value))


	print(len(set(stations)), ": ", stations)
	exit()

	for k, v in year.items():
		print(k, v, sep=": ")

	year_vals = {}
	for y, l in year.items():
		year_vals[y] = l

	print(year_vals)
	exit()

	plt.bar(range(len(year_vals)), year_vals.values(), align='center')
	plt.xticks(range(len(year_vals)), year_vals.keys())

	plt.show()

if __name__ == "__main__":
	main()
