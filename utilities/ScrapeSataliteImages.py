import urllib.request
from time import time
from time import sleep
import shutil
from datetime import datetime, timedelta
import zipfile
import os
import argparse
import logging

def scrape_image(date: datetime, zip, path:str):
	""" scrape_imange(datetime, zipfile.ZipFile, str)
		Scrapes a satalite image from https://atmos.washington.edu/images/vis1km/
		and saves it to the passed zip file.

		Args:
			date(datetime): The exact day and time to scrape the image from.
			zip(zipfile.ZipFile): The zipfile to write the image to.
			path(str): The path that the zipfile exists in.
	"""

	#: Log whats going on incase of later bugs.
	logging.info("Generating url to grab image from.")

	#: Generate the name that the website will store the file as.
	#: the format is yyyyddHHMM.gif
	server_filename = "{}{}{}{}{}.gif"\
	.format(
		date.year,
		str(date.month).zfill(2),
		str(date.day).zfill(2),
		str(date.hour).zfill(2),
		str(date.minute).zfill(2)
	)

	#: Generate the url that the satalite image will be stored on.
	url = "https://atmos.washington.edu/images/vis1km/{}"\
	.format(server_filename)

	#: Create the filename that we will store the satalite image as.
	logging.info("Creating file name.")
	filename = path + date.strftime('%b_%d_%Y_%H:%M.gif')

	log_string = ""

	#: Attempt to get a stalite image from the website,
	#: If we fail to find an image, then log that, and
	#: move on.
	try:
		log_string += "Attempting to grab image from {}...".format(date.strftime('%b %d %Y %H:%M'))
		#: Get an image from the url and store it into image.gif
		with urllib.request.urlopen(url) as response, open(path + 'image.gif', 'wb')  as outfile:
			#: Actually store the image.
			shutil.copyfileobj(response, outfile)
			#: Write image.gif to the passed zipfile under the previously
			#: generated file name.
			zip.write(outfile.name, filename)
			#: Log that we found the image.
			log_string += "Imaged added to cache"
	except urllib.error.HTTPError as e:

		#: Ignore this error, it just means there is
		#: no image for this time. Log that we did not
		#: find the image at this time.
		log_string += "No image found.".format(date)

	#: Put the generated log message into the log file.
	logging.info(log_string)


def main(path: str):
	""" ScrapeSataliteImages.py(str):
		Attempts to scrape one week's worth of satalite images from
		https://atmos.washington.edu/images/vis1km/ and store them in
		a zip file located at the passed path.

		Args:
			path(str): The path to save the zipfile in.
	"""

	#: Create a log file.
	logging.basicConfig(filename= path + "most_recent_log.txt", level=logging.DEBUG, filemode='w')

	#: Log whats going on incase of bugs.
	logging.info("Creating the date range.")
	#: Generate the last time to try and download an image from.
	end_date = datetime.fromtimestamp(time()) + timedelta(days = 1)
	end_date -= timedelta(hours = end_date.hour, minutes = end_date.minute)

	#: Get the first time to try and download an image from.
	day = datetime.fromtimestamp(time())
	day -= timedelta(days = 7, hours=day.hour, minutes=day.minute, seconds=day.second)

	#: Create a zipfile for this week's worth of satalite data.
	zipname = path + "Cloud_images_{}-{}.zip".format(day.strftime('%b_%d_%Y'), end_date.strftime('%b_%d_%Y'))

	#: log whats going on incase of bugs.
	logging.info("Creating zipfile")

	#: Create a zipfile to write all of the salalite images to.
	with zipfile.ZipFile(zipname, 'w') as zip:

		#: While the day datetime object (the one that is iterating through the
		#: week worth of minutes) is at an earier time than the end_date datetime
		#: object (the final time to collect data) then attempt to grab an image
		#: from the website
		while day <= end_date:
			#: Get the image for the current day, and write it to
			#: this zipfile.
			scrape_image(day, zip, path)

			#: Increase day by 5 minutes and attempt to get another image.
			day += timedelta(minutes=5)

		#: Write the logfile to the zip file.
		zip.write(path + "most_recent_log.txt", "log.txt")

	#: Log whats going on incase of bugs
	logging.info("Cleaning up working directory.")
	#: A 10 second delay to allow windows to stop using the files that
	#: are going to be removed from the working directory.
	sleep(10.0)

	#: Remove image.gif from the working directory.
	os.remove(path + "image.gif")

if __name__ == "__main__":

	#: Create an arugment parser.
	parser = argparse.ArgumentParser(description="Gets cloud images.")

	#: Create the path argument.
	parser.add_argument("path", type=str, help="The path to put the zipfile.")

	#: Parse the passed arguments.
	args = parser.parse_args()

	#: Run the collecor.
	try:
		main(args.path)
	except:
		logging.exception('')