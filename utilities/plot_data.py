import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import matplotlib
import numpy as np
import csv
import os
import random
import calendar
import MySQLUtilities
import datetime
from datetime import timedelta
import pickle

working_year = 2012

#:	{station_id: {datetime: data}}

def get_data(path: str, key_value: int=None):

	#: Gotten from a generated list of names.
	#usable_stations = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 15, 16, 17, 18, 19, 20, 21, 22, 24, 25, 26, 28, 29, 30, 31, 32, 34, 38, 39, 40, 42, 43, 55, 56, 58, 60, 61, 62, 63, 64, 65, 66, 68, 71, 73, 74, 75, 76, 77, 78, 79, 80, 87, 98, 107, 116, 142, 159, 162, 163, 164, 169, 170, 174, 175, 176, 181, 187, 188, 197, 198, 199, 200, 214, 215, 217, 227, 228, 229, 230, 231, 232]
	usable_stations =  [  6,   7,   8,   9,  10,  11,  12,  13,  16,  20,  25,  30,  31, 32,  34,  39,  40,  58,  61,  63,  64,  66,  68,  71, 73,  74,  75,  76,  78,  79,  80,  98, 107]

	missing_values = {

		#: Note positions 0 to 5 are required positions and are
		#: filled automatically by the program.
		6: "99.9",	 #: Field: Dry bulb temprature.
		7: "99.9",	 #: Field: Dew point temprature.
		8: "999",	 #: Field: Relative humidity.
		9: "101325",	 #: Field: Atmospheric station pressure.
		10: "9999",	 #: Field: Extraterrestrial horizontal radiation.
		11: "9999",	 #: Field: Extraterrestrial direct normal radiation.
		12: "9999",	 #: Field: Infrared radiation intensity.
		13: "9999",	 #: Field: global horizontal radiation.
		14: "9999",	 #: Field: Direct normal radiation.
		15: "9999",	 #: Field: Diffuse horizontal radiation.
		16: "999900",	 #: Field: Global horizontal illuminance.
		17: "999900",	 #: Field: Direct normal illuminance.
		18: "999900",	 #: Field: Diffuse horizontal illuminance.
		19: "9999",	 #: Field: Zenith luminance.
		20: "999",	 #: Field: Wind direction.
		21: "999",	 #: Field: Wind speed.
		22: "5",	 #: Field: Total sky cover.
		23: "5",	 #: Field: Opaque sky cover.
		24: "777.7",	 #: Field: Visibility.
		25: "77777",	 #: Field: Ceiling height.
		26: "9",	 #: Field: Present weather observations.
		27: "-999", 	 #: Field: Weather codes.
		28: "0",	 #: Field: Precipitable water.
		29: "0.0000",	 #: Field: Aerosol optical depts.
		30: "0",	 #: Field: Snow depth.
		31: "88",	 #: Field: Days since last snowfall.
		32: "999.000",	 #: Field: Albedo.
		33: "999.0",	 #: Field: Liquid precipitation depth
		34: "99.0" 	 #: Field: Liquid precipitation quantity
	}

	with open(path, 'r') as f:
		print("Reading data from {}".format(path))
		reader = csv.reader(f)
		raw_data = list(reader)[8:]
		station_name = path[path.find("at_") + 3: path.find(".epw")]

	station_id = MySQLUtilities.get_station_id(station_name)

	if not station_id in usable_stations:
		print("Ignoring this station as it is not in town")
		return "Dont use this"

	data_dict = {station_id: {}}

	if int(raw_data[8][0]) != working_year:
		print("Ignoring data as it is not in working year")
		return "Dont use this"

	if len(raw_data) <= 8760:
		print("File did not contain enough data")
		return "Dont use this"

	for row in raw_data:

		if len(row) == 0:
			continue

		data_date = datetime.datetime(int(row[0]), int(row[1]), int(row[2]), int(row[3]) - 1, 0)

		row_data = []

		if len(row[6:]) != 30:
			row_data = [None for i in range(30)]

		else:
			for index, item in enumerate(row[6:]):

				if len(item) == 0 or float(item) == float(missing_values[index + 6]) or "inf" in item or "nan" in item:
					row_data.append(None)
				else:
					row_data.append(float(item))
		data_dict[station_id].update({data_date: row_data})

	return [data_dict, station_id]

def avg(lst: list):
	return round(float(sum(lst) / len(lst)), 3) if len(lst) != 0 else 0

def internal_min(lst: list):
	return min(lst) if len(lst) != 0 else 0

def internal_max(lst: list):
	return max(lst) if len(lst) != 0 else 0

def fetch_all_data():

	data = {}

	for file in [os.path.abspath("weather_files/" + f) for f in os.listdir("weather_files/") if f[-4:] == ".epw"]:
		internal_data = get_data(file)
# ----------------------------------------------
# Correct but slow way; only use this when we are ready to generate graphs
		if internal_data != "Dont use this":
			try:
				data[internal_data[1]].update(internal_data[0][internal_data[1]])
			except KeyError:
				data[internal_data[1]] = internal_data[0][internal_data[1]]
# ----------------------------------------------

# Incorrect but fast way, used to debug: -------
#		if internal_data != "Dont use this":
#			data.update(internal_data[0])
# ----------------------------------------------

	for station, station_data in data.items():
		for timestamp, dataset in station_data.items():
			if dataset == [None for i in range(30)]:
				#: Set the datapoint to the datapoint of the previous hour.
				delta = 1
				prev_hour = timestamp + timedelta(hours=delta)
				print("Copying data from nearby row for ", timestamp, " at station ", station)
				while prev_hour not in data[station].keys() or data[station][prev_hour] == [None for i in range(30)]:
					prev_hour = prev_hour - timedelta(hours=delta)

					if delta < 0:
						delta -= 1
					else:
						delta += 1

					delta *= -1
					prev_hour = prev_hour + timedelta(hours=delta)

				if abs(delta) < 10:
					data[station][timestamp] = data[station][prev_hour]

	return [data]

def setup_data(w_num, working_value, s_type, sort_type):
	sort_by = []
	#: Sort by the sum of the differences to average in each station.
	sort_by.append(lambda x: np.nansum(x[1]))

	#: Sort by the sum of the absolute value of the differences to average in each station.
	sort_by.append(lambda x: np.nansum([abs(val) for val in x[1]]))
	sort_by.append(sort_by[0])

	sort_by = sort_by[sort_type]

	#                  DB, RH, GH, WS
	ignore_stations = [[], [28, 71, 77 ,79 ,6 ,26 ,58 ,15 ,30 ,8], [], []]

	#: Guessed values
	vmax = [[3, 4, 4], [15, 35, 10], [50, 50, 500], [3, 1, 5]]
	vmin = [[-3, -4, -4], [-15, -25, -10], [-50, -25, -500], [-3, -1, -5]]

	#: The type of statistic to look at.
	stat_type = []
	stat_type.append((avg, vmax[w_num][s_type], vmin[w_num][s_type], sort_by)) #: take the mean.
	stat_type.append((internal_min, vmax[w_num][s_type], vmin[w_num][s_type], sort_by)) #: take the min.
	stat_type.append((internal_max, vmax[w_num][s_type], vmin[w_num][s_type], sort_by)) #: take the max.

	stat_type = stat_type[s_type]

	file_bits = []
	file_bits.append(("Mean_differences", "Absolute_Mean_differences", "Airport_differences")[sort_type])
	file_bits.append(("Daily_Mean", "Daily_Min", "Daily_Max")[s_type])
	file_bits.append(("Dry_Bulb", "RH", "Global_horiz_radiation", "Wind_Speed")[w_num])

	if not os.path.isfile("data.p"):
		print("Getting data...")
		data = fetch_all_data()
		print("Pickling data...")
		pickle.dump(data, open("data.p", "wb"))
	else:
		print("Reading pickled data")
		data = pickle.load(open("data.p", "rb"))
		print("Done reading")

	key_data = []
	#: 2012 was a leap year -_-
	averages = [[] for i in range(366)]

	stations = []

	#: Create a graph using dataset[0]'s point (ie the dry temprature)
	for station, station_data in data[0].items():
		if len(station_data.keys()) == 0 or int(station) in ignore_stations[w_num]:
			continue

		key_data.append([])
		stations.append(station)
		prev_day = sorted(list(station_data.keys()))[0].day
		all_data = station_data.items()
		all_data = sorted(all_data, key=lambda x: x[0])

		last_timestamp = None
		day = []
		for timestamp, dataset in all_data:
			if timestamp.year == working_year:
				if dataset[working_value] is not None:
					day.append(float(dataset[working_value]))
					if s_type == 0:
						averages[timestamp.timetuple().tm_yday - 1].append(float(dataset[working_value]))
				else:
					day.append(None)

			if prev_day != timestamp.day and timestamp.year == working_year:
				if day.count(None) >= 24:
					key_data[-1].append(None)
					day = []
				else:

					day = [d for d in day if d is not None]

					key_data[-1].append(stat_type[0](day))
					if s_type != 0:
						averages[timestamp.timetuple().tm_yday -1].append(stat_type[0](day))
					day = []

			prev_day = timestamp.day
			last_timestamp = timestamp

		if day.count(None) >= 24:
			key_data[-1].append(None)
			day = []
		else:
			day = [d for d in day if d is not None]

			key_data[-1].append(stat_type[0](day))
			if s_type != 0:
				averages[last_timestamp.timetuple().tm_yday -1].append(stat_type[0](day))

	for day in range(len(averages)):
		averages[day] = round(avg(averages[day]), 3)

	print()
	print("Averages: ", averages)
	print()

	print("Stations: ", stations, len(stations))
	print()

	len_list = [len(dataset) for dataset in key_data]
	print("Data in stations: ", len_list, len(len_list))
	print()

	mean_diff = []

	for index, dataset in enumerate(key_data):
		if len(dataset) != 0:
			mean_diff.append((stations[index], []))
			for datapoint_index, datapoint in enumerate(dataset):
				if datapoint is not None:
					val = (round(datapoint - averages[datapoint_index], 3))
					mean_diff[-1][1].append(val)
				else:
					mean_diff[-1][1].append(float("nan"))

	mean_diff = sorted(mean_diff, key=stat_type[3])

	for dataset in mean_diff:
		print("Dataset: ", dataset, stat_type[3](dataset))
		print()

	pickle.dump(mean_diff, open('2012_graphs/{}/{}_{}_plot_data.p'.format(file_bits[2], file_bits[2], file_bits[1]), 'wb'))

	fig = plt.figure(figsize=(8, 12))
	ax = fig.add_subplot(111)
	masked_array = np.ma.array([dataset[1] for dataset in mean_diff], mask=np.isnan([dataset[1] for dataset in mean_diff]))

	cmap = matplotlib.cm.bwr
	cmap.set_bad('black', 1.)

	ax.imshow(masked_array, cmap=cmap, vmax=stat_type[1], vmin=stat_type[2], interpolation='nearest', aspect='auto')

	ax.set_yticks(range(len(mean_diff)))
	ax.set_yticklabels([dataset[0] for dataset in mean_diff], ha='right')

	ax.set_xlabel("Day".format(working_year))
	ax.set_ylabel("Station ID")

	red_patch = mpatches.Patch(color='red', label='{} >= {}'.format(file_bits[0], stat_type[1]))
	blue_patch = mpatches.Patch(color='blue', label='{} <= {}'.format(file_bits[0], stat_type[2]))
	black_patch = mpatches.Patch(color='black', label="No data present")

	plt.legend(handles=[red_patch, blue_patch, black_patch])

	filename = "2012_graphs/{}/{}_{}_{}_{}.png".format(file_bits[2], file_bits[0], file_bits[1], file_bits[2], working_year)

#	plt.show()
	plt.savefig(filename, dpi=300)
#	exit()

if __name__ == "__main__":
	working_value = [0, 2, 7, 15]

	for w_num in range(4):
		for s_type in range(3):
			for sort_type in range(2):
				setup_data(w_num, working_value[w_num], s_type, sort_type)
