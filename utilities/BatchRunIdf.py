import os
import zipfile

def main():
	#: Create lists containing all of the file names of all epws
	#: and idfs in the directory.
	epws = [epw for epw in os.listdir() if epw[-4:] == ".epw"]
	idfs = [idf for idf in os.listdir() if idf[-4:] == ".idf"]

	#: Open a zipfile to store the contents in.
	with zipfile.ZipFile("EnergyPlus_Outputs.zip", 'w') as zip:

		#: Go through every combonation of epw and idf
		for epw in epws:
			for idf in idfs:

				#: Get a list of the current files in the directory
				cur_files = os.listdir()

				#: Create a name for the file that the data will be
				#: stored in.
				directory_name = "{}_with_{}/".format(idf[:-4], epw[:-4])

				#: Attempt to run the energyplus command.
				#:
				#: Please note, if this command doesn't work,
				#: then you must add energyplus's executable's
				#: directory to your path environmental variable.
				os.system("energyplus -w {} {}".format(epw, idf))
				zip.write("eplusout.mtr", directory_name + "eplusout.mtr")
				zip.write("eplusssz.csv", directory_name + "eplusssz.csv")
				os.remove("eplusout.mtr")
				os.remove("eplusssz.csv")


if __name__ == "__main__":
	main()
