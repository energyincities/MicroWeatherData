import matlabfunctions
import datetime
import openpyxl
import csv
import calendar
from GetHourlyWeatherData import *
import os

def main():

	missing_values = {

		#: Note positions 0 to 5 are required positions and are
		#: filled automatically by the program.
		6: "99.9",	 #: Field: Dry bulb temprature.
		7: "99.9",	 #: Field: Dew point temprature.
		8: "999",	 #: Field: Relative humidity.
		9: "101325",	 #: Field: Atmospheric station pressure.
		10: "9999",	 #: Field: Extraterrestrial horizontal radiation.
		11: "9999",	 #: Field: Extraterrestrial direct normal radiation.
		12: "9999",	 #: Field: Infrared radiation intensity.
		13: "9999",	 #: Field: global horizontal radiation.
		14: "9999",	 #: Field: Direct normal radiation.
		15: "9999",	 #: Field: Diffuse horizontal radiation.
		16: "999900",	 #: Field: Global horizontal illuminance.
		17: "999900",	 #: Field: Direct normal illuminance.
		18: "999900",	 #: Field: Diffuse horizontal illuminance.
		19: "9999",	 #: Field: Zenith luminance.
		20: "999",	 #: Field: Wind direction.
		21: "999",	 #: Field: Wind speed.
		22: "5",	 #: Field: Total sky cover.
		23: "5",	 #: Field: Opaque sky cover.
		24: "777.7",	 #: Field: Visibility.
		25: "77777",	 #: Field: Ceiling height.
		26: "9",	 #: Field: Present weather observations.
		27: "-999", 	 #: Field: Weather codes.
		28: "0",	 #: Field: Precipitable water.
		29: "0.0000",	 #: Field: Aerosol optical depts.
		30: "0",	 #: Field: Snow depth.
		31: "88",	 #: Field: Days since last snowfall.
		32: "999.000",	 #: Field: Albedo.
		33: "999.0",	 #: Field: Liquid precipitation depth
		34: "99.0" 	 #: Field: Liquid precipitation quantity
	}

	averages = [[0.0 for _ in range(35)] for __ in range(8784)]
	num_averages = [[0.0 for _ in range(35)] for __ in range(8784)]

	for file in ["../weather_files/" + file for file in os.listdir("../weather_files/") if "2012_to_Jan_2013" in file and file[-4:] == ".epw"]:
		print(file)
		with open(file, 'r') as f:
			reader = csv.reader(f)
			raw_data = list(reader)[8:]

			for row in raw_data:
				if len(row) == 0:
					continue
				date = datetime(int(row[0]), int(row[1]), int(row[2]), int(row[3]) - 1, 0)

				for index, item in enumerate(row[6:], start=6):
					if len(item) == 0 or float(item) == float(missing_values[index]) or "inf" in item or "nan" in item:
						pass
					else:
						averages[(date.timetuple().tm_yday - 1) * 24 + date.hour][index] += float(item)
						num_averages[(date.timetuple().tm_yday - 1) * 24 + date.hour][index] += 1

	station_info = "LOCATION,Average,BC,CAN,WYEC2-B-24297,717990,0,0,-8.0,1,"

	base_time = datetime(2012, 1, 1, 0)
	epw_string = station_info + "\nDESIGN CONDITIONS,0,\nTYPICAL/EXTREME PERIODS,0,\nGROUND TEMPERATURES,0,\nHOLIDAYS/DAYLIGHT SAVING,No,0,0,0,\nCOMMENTS 1,,\nCOMMENTS 2,,\nDATA PERIODS,1,1,\n" #: A string to be built into the csv file.
	for hour_data in averages:
		epw_string += str(base_time.year) + ","
		epw_string += str(base_time.month) + ","
		epw_string += str(base_time.day) + ","
		epw_string += str(base_time.hour + 1) + ",0,"
		epw_string += "?9?9?9?9E0?9?9?9*9*9?9*9?9*_?9?9*9*9*9*_*9*9*9*9*9,"

		for hour_index, hour in enumerate(hour_data[6:]):
			if float(hour) == 0:
				epw_string += missing_values[hour_index + 6] + ","
			else:
				doy = (base_time.timetuple().tm_yday - 1) * 24 + base_time.hour
				epw_string += str(round(averages[doy][hour_index + 6] / num_averages[doy][hour_index + 6], 2)) + ","

		base_time += timedelta(hours=1)
		epw_string += "\n"

	with open('Average_data_for_Jan_2012_to_Jan_2013.epw', 'w+') as f:
		f.write(epw_string)
if __name__ == "__main__":
	main()
