try:
	import MySQLdb
except ImportError as e:
	print("You dont have MySQLdb installed. To isntall, please following these steps:\n")
	print("""
	+1.1  (for Ubuntu) From terminal run "sudo apt-get install
                   python-pip python-dev libmysqlclient-dev".

	1.2  (for Fedora)  From terminal run "sudo dnf install
                   python python-devel mysql-devel redhat-rpm-config gcc".

	1.3  (for Mac)     From termincal run "brew install mysql-connector-c".
                   Should that fail run "brew install mysql".

	1.4  (for Windows) Go to https://packaging.python.org/tutorials/installing-packages/
			   and follow the install instructions. The package name is "mysqlclient".
			   do NOT follow to step 2.

	2.  From terminal run "pip install mysqlclient".
	""")
	exit()


def get_db_connection(host, user, password, db):
#	print("host: ", host)
#	print("user: ", user)
#	print("password: ", password)
#	print("db: ", db)
	cnx = MySQLdb.connect(host=host,
                      	user=user,
                      	passwd=password.strip(),
                      	db=db)

	return cnx

def get_sensor_ids(db: MySQLdb.Connection, stationid: int):

	query = "SELECT id FROM sensor WHERE stationid={}".format(stationid)
	cur = db.cursor()

	#: Get the sensors ids from the stations that we're reading data
	try:
		cur.execute(query)
		sensors = cur.fetchall()
		sensors = [item for val in sensors for item in val]
		return(sensors)
	except:
		return None
	finally:
		db.close()
		cur.close()

def get_pass() -> str:

	with open("mysqlpassword.txt") as passfile:
		pw = passfile.readline()
	return pw


def get_station_id(station_name: str):
	pw = get_pass()
	db = get_db_connection("morze.seos.uvic.ca", "evins", pw, "meteo")

	query = "SELECT id FROM station WHERE name=\"{}\" LIMIT 1".format(station_name)
	cur = db.cursor()
	try:
		cur.execute(query)
		station_id = cur.fetchall()[0]

		return station_id[0]
	except:
		return None
	finally:
		db.close()
		cur.close()

def get_station_name(stationid: int):

	pw = get_pass()
	db = get_db_connection("morze.seos.uvic.ca", "evins", pw, "meteo")

	query = "SELECT name FROM station WHERE id={} LIMIT 1".format(stationid)
	cur = db.cursor()
	try:
		cur.execute(query)
		station_name = cur.fetchall()[0]
		cur.close()

		return(station_name[0])
	except:
		return None
	finally:
		db.close()
		cur.close()

def get_all_station_ids():

	pw = get_pass()
	db = get_db_connection("morze.seos.uvic.ca", "evins", pw, "meteo")

	query = "SELECT id FROM station"
	cur = db.cursor()
	cur.execute(query)

	cur.close()
	db.close()

	return [id[0] for id in cur.fetchall()]


def get_station_coordinates(stationid: int):

	pw = get_pass()
	db = get_db_connection("morze.seos.uvic.ca", "evins", pw, "meteo")

	#: Get and set the coordinates.
	query = "SELECT longitude, latitude, elevation "\
		"FROM stn_coordinates WHERE id={}".format(stationid)

	cur = db.cursor()
	try:
		cur.execute(query)
		station_data = cur.fetchall()[0] #: Lat, long, and elev.
		station_long = station_data[0] #: Long.
		station_lat = station_data[1] #: Lat.
		station_elev = station_data[2] #: Elev.

		#: Convert longitude to be between -180 and 180.
		if station_long > 180:
			station_long += -360

		#: Convert latitude to be between -90 and 90.
		if station_lat > 90:
			station_lat += -180

		return (round(station_lat, 3), round(station_long, 3), round(station_elev, 3))
	except:
		return (None, None, None)
	finally:
		db.close()
		cur.close()

def execute_query(query: str):
	pw = get_pass()
	db = get_db_connection("morze.seos.uvic.ca", "evins", pw, "meteo")

	cur = db.cursor()

	try:
		cur.execute(query)
		vals = cur.fetchall()
	except Exception as e:
		print(query + " was an invalid query:\n" , e)
		return(None)

	finally:
		db.close()
		cur.close()

	vals = [item for item in vals]

	for index, item in enumerate(vals):
		if len(item) == 1:
			vals[index] = item

	return vals


def close_connection(db: MySQLdb.Connection):
	db.close()

if __name__ == "__main__":
	cnx = get_db_connection()
	print(get_sensor_ids(cnx, 1))
	print(get_station_name(cnx, 1))
	print(get_station_coordinates(cnx, 1))
	print(execute_query(cnx, "SELECT * FROM stata"))
	print(execute_query(cnx, "SELECT * FROM sdata LIMIT 1"))
	close_connection(cnx)
