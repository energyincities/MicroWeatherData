""" Reference:
The calculations for both solar duration and solar altitude were
extracted from the excel files provided here:
	https://www.esrl.noaa.gov/gmd/grad/solcalc/calcdetails.html
"""

import datetime
from math import degrees as DEGREES
from math import acos as ACOS
from math import sin as SIN
from math import radians as RADIANS
from math import cos as COS
from math import fmod as MOD
from math import atan2 as ATAN2
from math import asin as ASIN
from math import tan as TAN
from math import pi as PI

def calc_sol_dur(lat: float, long: float, time_zone: int, date: datetime):
	B3 = lat
	B4 = long
	B5 = time_zone
	B7 = date

	D_mid = B7 - datetime.datetime(1900, 1, 1, 0)
	D = D_mid.days + 2
	E = (B7.hour * 60 + B7.minute) / 1440
	F = D+2415018.5+E-B5/24
	G = (F-2451545) / 36525
	I = MOD(280.46646+G*(36000.76983+G*0.0003032),360)
	J = 357.52911+G*(35999.05029-0.0001537*G)
	K = 0.016708634-G*(0.000042037+0.0000001267*G)
	L = SIN(RADIANS(J))*(1.914602-G*(0.004817+0.000014*G))+SIN(RADIANS(2*J))*(0.019993-0.000101*G)+SIN(RADIANS(3*J))*0.000289
	M = I + L
	N = J + L
	O = (1.000001018*(1-K*K))/(1+K*COS(RADIANS(N)))
	P = M-0.00569-0.00478*SIN(RADIANS(125.04-1934.136*G))
	Q = 23+(26+((21.448-G*(46.815+G*(0.00059-G*0.001813))))/60)/60
	R = Q+0.00256*COS(RADIANS(125.04-1934.136*G))
	T = DEGREES(ASIN(SIN(RADIANS(R))*SIN(RADIANS(P))))
	U = TAN(RADIANS(R/2))*TAN(RADIANS(R/2))
	V = 4*DEGREES(U*SIN(2*RADIANS(I))-2*K*SIN(RADIANS(J))+4*K*U*SIN(RADIANS(J))*COS(2*RADIANS(I))-0.5*U*U*SIN(4*RADIANS(I))-1.25*K*K*SIN(2*RADIANS(J)))
	W = DEGREES(ACOS(COS(RADIANS(90.833))/(COS(RADIANS(B3))*COS(RADIANS(T)))-TAN(RADIANS(B3))*TAN(RADIANS(T))))
	X = (720-4*B4-V+B5*60)/1440
	Y = X-W*4/1440
	Z = X+W*4/1440
	AA = 8 * W

	return AA

def calc_sol_alt(lat: float, long: float, time_zone: int, date: datetime):
	B3 = lat
	B4 = long
	B5 = time_zone
	B7 = date

	D_mid = B7 - datetime.datetime(1900, 1, 1, 0)
	D = D_mid.days + 2
	E = (B7.hour * 60 + B7.minute) / 1440
	F = D+2415018.5+E-B5/24
	G = (F-2451545) / 36525
	I = MOD(280.46646+G*(36000.76983+G*0.0003032),360)
	J = 357.52911+G*(35999.05029-0.0001537*G)
	K = 0.016708634-G*(0.000042037+0.0000001267*G)
	L = SIN(RADIANS(J))*(1.914602-G*(0.004817+0.000014*G))+SIN(RADIANS(2*J))*(0.019993-0.000101*G)+SIN(RADIANS(3*J))*0.000289
	M = I + L
	N = J + L
	O = (1.000001018*(1-K*K))/(1+K*COS(RADIANS(N)))
	P = M-0.00569-0.00478*SIN(RADIANS(125.04-1934.136*G))
	Q = 23+(26+((21.448-G*(46.815+G*(0.00059-G*0.001813))))/60)/60
	R = Q+0.00256*COS(RADIANS(125.04-1934.136*G))
	T = DEGREES(ASIN(SIN(RADIANS(R))*SIN(RADIANS(P))))
	U = TAN(RADIANS(R/2))*TAN(RADIANS(R/2))
	V = 4*DEGREES(U*SIN(2*RADIANS(I))-2*K*SIN(RADIANS(J))+4*K*U*SIN(RADIANS(J))*COS(2*RADIANS(I))-0.5*U*U*SIN(4*RADIANS(I))-1.25*K*K*SIN(2*RADIANS(J)))
	W = DEGREES(ACOS(COS(RADIANS(90.833))/(COS(RADIANS(B3))*COS(RADIANS(T)))-TAN(RADIANS(B3))*TAN(RADIANS(T))))
	X = (720-4*B4-V+B5*60)/1440
	Y = X-W*4/1440
	Z = X+W*4/1440
	AA = 8 * W
	AB = MOD(E*1440+V+4*B4-60*B5,1440)
	AC = AB/4+180 if AB/4<0 else AB/4-180
	AD = DEGREES(ACOS(SIN(RADIANS(B3))*SIN(RADIANS(T))+COS(RADIANS(B3))*COS(RADIANS(T))*COS(RADIANS(AC))))
	AE = 90-AD
	return AE


if __name__ == "__main__":
	sol_alt = calc_sol_alt(40, -105, -5, datetime.datetime(2018, 1, 1, 0, 6))
	print(sol_alt)