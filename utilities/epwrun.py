import os
import pickle
from MySQLUtilities import *

def main():
	for file in ["epwrun/" + file for file in os.listdir("epwrun/") if file[-2:] == ".p"]:
		new_data = {}
		data = pickle.load(open(file, 'rb'))

		for key, value in data.items():
			new_data[key] = {}
			for k, v in value.items():
				new_data[key].update({get_station_id(k): v})

		print(new_data)
		pickle.dump(new_data, open(file, 'wb'))

if __name__ == "__main__":
	main()
