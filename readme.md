Usage Instructions:

	GetHourlyWeatherData:

		Purpose:
			This python script will access the MySQL database, and pull
			out relevant information for the dates provided in the Parameters
			for the station provided in the Parameters.

			The gathered data will then be outputted into annual epw files
			for each year of gathered data, and one large csv containing all
			of the information for all of the years. csv-epw.py can convert these
			csvs into working epw files, so storage of the epw's is not necessary.


		Parameters:

			GethourlyWeatherData.py's command line interface has 2 required
			Parameters, and based on the second parameter, has 4 or 2 more following.

			1: station_id:
				This is the id of the station that is stored
				in the database. If you are unsure about
				the ID of the station that you would like to
				get data from, please run FindStationLifespans.py.
				That script will output not only a list of station
				names and ids, but also the date for when the station
				first became active, and when it went offline.

			2: --by_list (-l)  or --by_range (-r):
				This determines the behavior of the search. If --by_range
				is selected, then the program will search for data between
				two specified dates; if --by_list is selected, then data
				will be searched for in specific months of specific years.

			Arguments if --by_range (-r) is selected:

				1: --from_year (-fy):
					this is the earliest year to start searching for
					data.

				2: --to_year (-ty):
					this is the last year to search for data in.

				3: --from_month (-fm):
					this is the first month to search for data in.
					Represented by the number of the month
	
				4: --to_month (-tm):
					this is the last month to search for data in.
					Represented by the number of the month.

				Example run:

					"python3 GetHourlyWeatherData.py 1 -r -fy 2016 -ty 2017 -fm 1 -tm 12"
				
					This finds all of the data between and including January 2016 to 
					December 2017.

			Arguments if --by_list (-l) is selected:

				1: --years (-y):
					A list of the years to search for data in.

				2: --months (-m):
					A list of the number of each month to search for data in within
					the selected years.

				Example run:

					"python3 GetHourlyWeatherData.py 1 -l -y 2015 2016 2017 -m 1 2 3"

					This finds all of the data for January, February, and March in
					2015, 2016 and 2017. Empty data will be generated for the rest.



	CollectAllWeatherData:

		Purpose:
			This file will make a call to GetHourlyWeatherData for each station for each
			year from 2004 on.
			Each of these call's data is built into an epw file. Code exisits to build csv
			files, though there may be issues with the generated data (no recent tests for
			csv functions.)
			
		Parameters:
			
			-p, --password: 
				The password to the MySQL database to read the data from.
		
	Utilities:
	
		csv-epw:
		
		
			Purpose:
				Takes in a csv file, and writes out epw files based off the data read from the csv.
			
				The functionality of this file needs testing, as it has not been used recently.
			
			Parameters:
				Filenames:
					Self explanitory; The file names of the csvs to be converted.
	
		FindStationLifespans:
	
			Purpose:
				Creates a file to show which stations have data when.
			
			Parameters:
				-p, --password:
					The password to the MySQL database to read the data from.
	
		BatchRunIdf:
	
			Purpose:
				Runs all csv files against all epw files in the directory, and stores
				their results into zipfiles.
				
	
		MySQLUtilities:
	
			Purpose:
				An internally used library of useful MySQL functions for
				interacting with the MySQL database.
	
		ScrapeSataliteImages:
	
			Purpose:
				Scrapes image data from https://atmos.washington.edu/images/vis1km/
				and stores it in a zipfile.
				
			Parameters:
				Path:
					The location to save each zipfile of images.
