#: default libraries
from datetime import datetime, timedelta
from time import time
import csv
import calendar
import argparse
from math import radians

#: Non-default libraries
try:
	import MySQLdb
except ImportError as e:
	print("You dont have MySQLdb installed. We will attempt to install it automatically...\n"\
	      "If an error occurs relating to \"permission denied\" then please run as admin.")
	import subprocess
	subprocess.check_call(["python3", "-m" ,"pip", "install", "--user", "mysqlclient"])
	subprocess.check_call(["python3", "-m", "pip",  "install", "--user", "--upgrade", "mysqlclient"])
	print("Sucessfully installed MySQLdb. Please run the program again.")
	exit()

try:
	#: Thank you to Matt Eames, M.E.Eames@exeter.ac.uk, for providing
	#: both the matlab functions, and support on their usage in this
	#: program for calculating missing values used by E+.
	import matlabfunctions
except ImportError as e:
	print("You do not have the required matlab functions package installed.\n"\
	      "We will attempt to install it automatically...\n"\
	      "if an error occurs relating to \"permission denied\" then please run as admin")

	import subprocess
	subprocess.check_call(["python3", "matlabfunctions/for_redistribution_files_only/setup.py", "install"])
	print("Sucessfully installed the matlab functions. Please run the program again.")
	exit()
try:
	import openpyxl
except ImportError as e:
	print("You dont have openpyxl installed. We will attempt to install it automatically...\n"\
	      "If an error occurs relating to \"permission denied\" then please run as admin.")
	import subprocess
	subprocess.check_call(["python3", "-m", "pip", "install", "--user", "openpyxl"])
	subprocess.check_call(["python3", "-m", "pip", "install", "--user", "--upgrade", "openpyxl"])

	print("Sucessfully installed openpyxl. Please run the program again.")
	exit()

from openpyxl import Workbook

#: Custom libraries
from utilities.CalcSolAlt import calc_sol_alt, calc_sol_dur
from utilities.MySQLUtilities import *

""" Please Note:
Should you be unable to run this program, you
may still need the MySQLdb module, or you might
be running this from a computer that doesn't
have permission to access the database.

If you need to install the MySQLdb library (the error python
gives you will look something like ImportError: no module named
"MySQLdb") then follow the following.
Steps:
1.1  (for Ubuntu)  From terminal run "sudo apt-get install
		   python-pip python-dev libmysqlclient-dev".

1.2  (for Fedora)  From terminal run "sudo dnf install
	           python python-devel mysql-devel redhat-rpm-config gcc".

1.3  (for Mac) 	   From termincal run "brew install mysql-connector-c".
	           Should that fail run "brew install mysql".
+1.1  (for Ubuntu)  From terminal run "sudo apt-get install
                   python-pip python-dev libmysqlclient-dev".

1.2  (for Fedora)  From terminal run "sudo dnf install
                   python python-devel mysql-devel redhat-rpm-config gcc".

1.3  (for Mac)     From termincal run "brew install mysql-connector-c".
                   Should that fail run "brew install mysql".

2.  From terminal run "pip install mysqlclient".

TODO:

	- The current MySQL call made does a poor job. It pulls all of the
	  data for all valid timestamps and moved on. This gives waay to
	  issues involving missing data, and very very very slow
	  data extraction. According to Ed, there is are tables in the
	  MySQL database with the name stats_YYYY. This will contain hourly
	  data theoretically (Talk to Ed about it if the data doesnt seem to
	  work.

"""

def get_hourly_weather_data(
	dbpass: str,
	station: int = 189,
	from_date: datetime = None,
	to_date: datetime = None,
	years: list = None,
	months: list = None
	):
	""" main(int, datetime, datetime, list, list):

	The main function of the program. It will generate a csv file in the format
	required by a epw file based off of collected data from the SQL database for
	the passed station, year, and month (alternatively if today is set to true then
	it will generate based off of today's data rather than that of a given year and
	month.

	Args:
		station(int): 		The id of the station to get data from.

		from_date(datetime): 	The first date to start gathering data from
					in a range. If from_date and to_date are both
					not none, then data will be taken from all
					months from from_date to to_date (not including the
					last month of to_date.

		to_date(datetime):	The last date to gather data from. This should always
					be the first day of the month after the last month
					you would like to collect data from. If from_date and to_date are both
					not none, then data will be taken from all
					months from from_date to to_date (not including the
					last month of to_date.

		months(list):		Used if either from_date, to_date, or both are None.
					a list of months that we will gather data from for
					each year in years.

		years(list):		Used if either from_date, to_date, or both are None.
					a list of years to gather data from for each month
					in months.

	Note:
		Either from_date and to_date, or months and years must both be passed.
		If neither of these pairs are passed (or only partial pairs are passed)
		then an error will be thrown and the program will terminate.
	"""

	print("Initializing necessary structures.")

	#: Create the matlab functions object.
	mlfuncs = matlabfunctions.initialize()

	#: Get the csv file to read cloud data from, and find its length
	cloud_file = open("reference_files/cloud_data.csv", 'r')

	#: Create the variables to hold the information on the file.
	cloud_file_length = 0
	cloud_lines = []
	offset = 0

	for line in cloud_file:
		#: Append the current offset (offset is the variable holding the
		#: position of the first character of each line of the file, that
		#: way we can have a O(1) time to read from the file after this one
		#: O(N) time setup.
		cloud_lines.append(offset)
		#: Add one to the number of lines in the file.
		cloud_file_length += 1
		#: Add the length of the line + 1 for the newline to
		#: offset.
		offset += len(line) + 1

	#: Set the file's cursor back to position 0
	cloud_file.seek(0)

	#: Establish the connection to the database
	print("Connecting to SQL database.")
	db = get_db_connection("morze.seos.uvic.ca", "evins", dbpass, "meteo")

	#: start a db cursor and a file pointer
	print("Creating a database cursor.")
	cur = db.cursor()

	#: Get the sensors ids from the stations that we're reading data
	print("Getting sensors for selected station.")
	query = "SELECT id, name FROM sensor WHERE stationid={}".format(station)
	cur.execute(query)
	sensors = cur.fetchall()

	if sensors is None:
		print("No sensors found")
		return None

	#: Get the station's name, and coordinates to properly generate the
	#: header for the epw files.
	print("Gathering information about the station.")

	#: Get the station's name
	station_name = get_station_name(station)
	if station_name is None:
		print("No station name found")
		return None

	#: Get the station's coordinates
	station_coords = get_station_coordinates(station)
	if station_coords is None:
		print("No coordinates found")
		return None
	station_lat = station_coords[0]
	station_long = station_coords[1]
	station_elev = station_coords[2]

	#: A list containing the 2 digit month codes.
	months_list = ["01", "02", "03", "04", "05", "06",
		      "07", "08", "09", "10", "11", "12"]

	time_dict = {} #: Used to determine which years and months
		       #: to pull data from.

	filenames = [] #: A list to store the seperate filenames.

	#: If a from date and to date are not given
	#: Create the dictionary of dates to pull by saying
	#: we will pull the same months from each year.
	if (from_date is None or to_date is None) and (months is None or years is None):
		print("Fatal Error: neither pair of from_date and to_date, or\n"\
		      "		    years and months are complete. Cannot collect\n"\
		      "		    data as not enough information on what data to\n"\
		      "		    collect was provided. See doccumentation of main.")
		exit()


	elif from_date is None or to_date is None:

		#: Sort the months and years to gurantee that they are in
		#: chomatic order.
		months = sorted(months, key=lambda x: int(x))
		years = sorted(years, key=lambda x: int(x))

		#: Assosiate all passed months with each year.
		for year in years:

			#: Each key in the dict is a year, and each value is
			#: the months to assosiate with each year.
			time_dict[year] = months

			#: Generate the filename of each year's worth of data.
			file = "weather_data_for_{}_{}-{}_{}_at_{}.epw".format(
					calendar.month_abbr[int(months[0])],
					year,
					calendar.month_abbr[int(months[-1])],
					year,
					station_name
				)
			filenames.append(file)

	else:
		#: Calculate the number of years within the range.
		number_of_years = to_date.year - from_date.year

		#: Create different file names based on how many years of data we want to collect.
		if number_of_years == 0:
			filenames.append("weather_data_for_{}_{}-{}_{}_at_{}.epw".format(
						calendar.month_abbr[from_date.month],
						from_date.year,
						calendar.month_abbr[to_date.month],
						to_date.year,
						station_name
					))
		else:
			filenames.append("weather_data_for_{}_{}-Jan_{}_at_{}.epw".format(
						calendar.month_abbr[int(from_date.month)],
						from_date.year,
						from_date.year + 1,
						station_name
					))

		#: For any years that are done in whole, assosiate every month
		#: with those years.
		if number_of_years >= 2:
			for i in range(1, number_of_years):
				time_dict[from_date.year + i] = months_list
				filenames.append("weather_data_for_Jan_{}-Jan_{}_at_{}.epw".format(
							from_date.year + i,
							from_date.year + i + 1,
							station_name
						))

		filenames.append("weather_data_for_Jan_{}-{}_{}_at_{}.epw".format(
							to_date.year,
							calendar.month_abbr[int(to_date.month)],
							to_date.year,
							station_name
						))

		#: Assosiate the edge years with the months within the ranges
		#: passed in to_date and from_date.
		time_dict[from_date.year] = months_list[int(from_date.month) - 1:]
		time_dict[to_date.year] = months_list[:int(to_date.month)]

	# String generated by following format:
	#:	 "LOCATION,Victoria Int'l,BC,CAN,WYEC2-B-24297,717990,48.65,-123.43,-8.0,19.0,"
	#:
	station_info = "LOCATION,{},BC,CAN,WYEC2-B-24297,717990,{},{},-8.0,{},"\
			.format(station_name, round(station_lat, 2), round(station_long, 2), station_elev)

	sensor_data = [] #: Create the array to hold all of the sensor data.

	#: Build the query to get the rows from sdata from the station and
	#: year selected (Only grabs data from the first minute of each hour.
	for sensor in sensors:
		for year, umonths in time_dict.items():
			#: Years before 2005 cant be queried by month, and instead need
			#: to be queried by the whole year
			if year > 2005:
				for month in umonths:
					print("Gathering data for {} {} from {}'s {} sensor."\
						.format(calendar.month_name[int(month)], year, station_name, sensor[1]))
					#: Select the timekey, fieldid and value from the given date,
 					#: where the sensorid is equal to the given station, and the timekey
					#: is between the two generated times representing the beginning and
					#: end of the year.

					#: Query for the data for the current year and current month where the sensor
					#: id is the current id, and its the first minute of the hour.
					query = "SELECT timekey, fieldid, value FROM sdata_{}_{} WHERE sensorid={}"\
						" and (timekey/60) % 60 = 0".format(year, month, sensor[0])

					#: Get the data from this specific sensor.
					cur.execute(query)
					for item in cur.fetchall():
						#: Append the found data to the list containing the data
						sensor_data.append(item)

			else:
				print("Gathering data for {} from {}'s {} sensor."\
					.format(year, station_name, sensor[1]))

				#: Grab data for the current year, where the sensor id is the current id
				#: and its the first minute of the hour.
				query = "SELECT timekey, fieldid, value FROM sdata_{} WHERE sensorid={}"\
					" and (timekey/60) % 60 = 0".format(year, sensor[0])

				#: Execute the query
				cur.execute(query)
				for item in cur.fetchall():
					#: Append the found data to the list containing the data
					sensor_data.append(item)

	#: Sort the datapoints by their timestamps to ensure that
	#: all relevant readings are side by side.
	sensor_data = sorted(sensor_data, key=lambda x: x[0])

	if len(sensor_data) == 0:
		print("No data found in sensors")
		return None

	cur.close() #: Close the sql database's cursor.
	db.close() #: Close the connection as we no longer need to read
		   #: from the sql database.

	first_data = datetime.fromtimestamp(sensor_data[0][0])
	last_data = datetime.fromtimestamp(sensor_data[-1][0])
	filenames.insert(0, "weather_data_for_{}-{}_at_{}.csv".format(
								first_data.strftime('%b_%Y'),
								last_data.strftime('%b_%Y'),
								station_name
								))

	#: A dictionary to convert between the position of the row in
	#: in the mfield table, and the required position in the csv format.
	#:
	#: Should additional fields be added to the mfield table that can be
	#: recorded by EnergyPlus, they can be added to the dictionary in the
	#: following format:
	#:
	#:	[mfieldid of new addition] : [position the field's information is
	#:				      located in, within the epw.]
	#:
	#: Should a field need to be ignored, commenting out the line with its
	#: key-value pair will affectively ignore it. A reason should be added
	#: in comment beside the key-value pair's function.
	mfield_to_csv_loc = {
		0:6,   #: Field: Dry bulb temprature.
		5:7,   #: Field: Dew point temprature.
		10:8,  #: Field: Relative humidity.
		30:9,  #: Field: Atmospheric station pressure.
		40:13, #: Field: Global horizontal radiation.
#		51:33, #: Field: Liquid precipitation depth.	Ignored according to "weather file variables.xlsx"
#		52:34, #: Field: Liquid precipitation quantity.	Ignored according to "weather file variables.xlsx"
		60:21, #: Field: Wind speed
		61:20  #: Field: Wind direction
	}

	#: Conversion rates for different units
	mfield_conversion_ratios = {
		0: 1,
		5: 1,
		10: 1,
		30: 100,
		40: 1,
		51: 1,
		52: 1,
		60: 1,
		61: 1
	}

	#: PLEASE NOTE:
	#:	The current header prepended in this string is for testing purposes only.
	#:	in the final version of this program, some of this will be automatically
	#:	generated, and the rest will need to be passed in.
	epw_string = station_info + "\nDESIGN CONDITIONS,0,\nTYPICAL/EXTREME PERIODS,0,\nGROUND TEMPERATURES,0,\nHOLIDAYS/DAYLIGHT SAVING,No,0,0,0,\nCOMMENTS 1,,\nCOMMENTS 2,,\nDATA PERIODS,1,1,\n" #: A string to be built into the csv file.
	if __name__ == "__main__":
		csv_header = "Station id,Year,Month,Day,Hour,Minute,Data Source and Uncertaintly Flags,Dry Bulb Temprature,Dew Point Temprature,Relative Humidity,Atmospheric Station Pressure,Extraterrestrial Horizontal Radiation,Extraterrestrial Direct Normal Radiation,Horizontal Infrared Radiation Intensity,Global Horizontal Radiation,Direct Normal Radiation,Diffuse Horizontal Radiation,Global Horizontal Illuminance,Direct Normal Illuminance,Diffuse Horizontal Illuminance,Zenith Luminance,Wind Direction,Wind Speed,Total Sky Cover,Opaque Sky Cover,Visibility,Ceiling Height,Present Weather Observation,Present Weather Codes,Precipitable Water,Aerosol Optical Depth,Snow Depth,Days Since Last Snowfall,Albedo,Liquid Precipitation Depth,Liquid Precipitation Quantity"
		csv_string = csv_header + "\n{},".format(station)  #: Used to generate the csv file.
	else:
		csv_string = "{},".format(station)

	empty = [None for _ in range(35)] #: Used for an easy reset after each hour of data.

	#: Fill in the default information for the csv file.
	empty[0] = int(year)
	empty[1] = int(month)
	empty[4] = 0 #: The minutes is always the first of the hour.

	#: An empty string here is valid, for until better flags can be created this will work.
	#: "https://bigladdersoftware.com/epx/docs/8-8/auxiliary-programs/data-sources-uncertainty.html"
	empty[5] = "?9?9?9?9E0?9?9?9*9*9?9*9?9*_?9?9*9*9*9*_*9*9*9*9*9"

	hours = list(empty) #: Holds sensor data for each hour.

	date = datetime.fromtimestamp(sensor_data[0][0]) #: Convert from unixtime to useable.
	prev_date = date #: Used for keeping track of the last timestamp's date.
	first_month = str(date.month).zfill(2) #: Used later for filename generation.
	first_year = str(date.year) #: Used later for filename generation.
	hours[0] = str(date.year) #: Set the year to the year given in the timestamp.
	hours[1] = str(date.month) #: Set the month to the month given in the timestamp.
	hours[2] = str(date.day) #: Set the day to the day given in the timestamp.
	hours[3] = str(date.hour + 1) #: Set the hour to the hour given in the timestamp.

	epw_strings = []

	print("Generating csv and epw files.")
	#: Convert the rows format into a format where each sensor reading
	#: for each hour is stored in one container.
	if not (date.month == 1 and date.day == 1 and date.hour == 0):
		#: Need to remove all entries from earlier than year + 1's first day
		#: then try to get the frist date again.

		#: Currently this portion of the function generates empty data if the
		#: beggining of the year is missing, however in the future it should be re-written
		#: to exclude the year's worth of data.
		print("Pre-", end='')
		missing_entries = generate_missing_entries(station, datetime(date.year, 1, 1, 0), date)
#		for i in range(len(missing_entries[1]) % 8760):
#			filenames = filenames[1:]
		print("Generating missing values...")

		csv_string += missing_entries[1]
		epw_string += missing_entries[0]
	else:
		print("Generating missing values...")

	for data_index, data_point in enumerate(sensor_data):

		#: If the timestamps are not equal
		if data_index != 0 and sensor_data[data_index][0] != sensor_data[data_index - 1][0]:

			#: If entries dont exist straight through to the end of the year,
			#: then fill in the missing data with blank rows.
			if not ((prev_date + timedelta(hours=1)).year == date.year):
				if not (date.month == 1 and date.day == 1 and date.hour == 0):
					missing_entries = generate_missing_entries(station, prev_date, datetime(date.year, 1, 1, 0))

			#: Otherwise, fill in the missing gaps.
			elif (prev_date + timedelta(hours=1)).date() != date.date():
				missing_entries = generate_missing_entries(station, prev_date, date)

			#: Otherwise there are no missing entries for this period.
			else:
				missing_entries = ["", ""]

			#: Build the row of the string based on hours
			line = get_hour_data(hours, mlfuncs, cloud_file, cloud_file_length, cloud_lines, station_coords)
			epw_string += missing_entries[0]
			csv_string += missing_entries[1]

			#: Append newline and comma for next row.
			if prev_date.year == date.year:
				epw_string += line[0] + ',\n'
				csv_string += line[1]
			#: Otherwise this is a new year so we need tot generate another file.
			else:
				#: Generate the header at the top of the file
				epw_strings.append(epw_string)
				epw_string = station_info + "\nDESIGN CONDITIONS,0,\nTYPICAL/EXTREME PERIODS,0,\nGROUND TEMPERATURES,0,\nHOLIDAYS/DAYLIGHT SAVING,No,0,0,0,\nCOMMENTS 1,,\nCOMMENTS 2,,\nDATA PERIODS,0,1,\n" #: A string to be built into the csv file.

				#: Check if this is the first hour of the year
				if not (date.month == 1 and date.day == 1 and date.hour == 0):
					#: Note, as this code is, it will generate missing data for the year,
					#: however it should just throw out the year.
					print("Pre-", end='')
					missing_entries = generate_missing_entries(station, datetime(date.year, 1, 1, 0), date)
					csv_string += missing_entries[1] + line[1]
					epw_string += missing_entries[0] + line[0]
					print("Generating missing values...")
				else:
					#: Otherwise append the first line of data to the file
					csv_string += line[1]

			#: Prepend the station number to the line of data in the csv string.
			csv_string += '\n{},'.format(station)

			#: Reset hours for the next run.
			hours = list(empty)
			prev_date = date
			date = datetime.fromtimestamp(data_point[0]) #: Convert from unixtime to useable.
			hours[0] = str(date.year) #: Set the year to the year given in the timestamp.
			hours[1] = str(date.month) #: Set the month to the month given in the timestamp.
			hours[2] = str(date.day) #: Set the day to the day given in the timestamp.
			hours[3] = str(date.hour + 1) #: Set the hour to the hour given in the timestamp.

		#: If the given fieldid is in mfiled_to_csv_loc
		if data_point[1] in mfield_to_csv_loc.keys():

			#: Replace the current value of hours to the one from the query.
			actual_index = mfield_to_csv_loc[data_point[1]]
			hours[actual_index] = data_point[2] * mfield_conversion_ratios[data_point[1]]

	#: Get the data for this hour.
	line = get_hour_data(hours, mlfuncs, cloud_file, cloud_file_length, cloud_lines, station_coords) #: Build the last line of data.

	#: Write the data for the last hour we have, and then add in the missing values.
	epw_string += line[0] + ","  + missing_entries[0] #: Build the last rows of the epw string.
	csv_string += line[1]  + missing_entries[1] #: Build the last rows of the csv string.

	#: Get the time for the current sensor data.
	date = datetime.fromtimestamp(sensor_data[-1][0] + 3600)

	#: If this is not the last hour of the year
	if hours[1] != "12" or hours[2] != "31" or hours[3] != "24":
		#: Generate the missing data, and write it to the strings.
		missing_entries = generate_missing_entries(station, date, datetime(date.year, 12, 31, 23), prepend_newl=True)
		epw_string += missing_entries[0]
		csv_string += missing_entries[1][:-3]

	#: Append the the epw string to the list of epw string.
	epw_strings.append(epw_string)

	#: Warn used about the required generated data for months not within
	#: the months array when there is more than one year.
	if len(time_dict) > 1 and ((from_date is None or to_date is None) and len(months) != 12):
		print("""\033[0;31m
Warning: data for months between {} {} and {} {} (not including any
months that were entered by the user between the two years) was generated
in order to meet EnergyPlus's requirements. Those months have been filled
with empty data.
		      \033[0m""".format(
					calendar.month_name[int(months[-1])],
					years[-1],
					calendar.month_name[int(months[0])],
					years[0]
				))

	#: End the matlab functions object, and return the data gathered.
	mlfuncs.terminate()

	cloud_file.close() #: We have read all the data from the cloud file that we'll need.

	return [epw_strings, csv_string, filenames]


def write_data_to_files(epws: list, filenames:list, csv: str=None):
	""" write_data_to_file(str, list, list)

	Writes the csv and epw string to respective .csv and .epw files, who's names are
	generated based on the passed datetime objects.

	Args:
		csv(str): The string to write to the .csv file

		epws(list): A list of the strings to write to the .epw file

		filename(list):  a list of the names of the files to be created.
				 filename[0] is the name of the csv file, the
				 rest (filename[1:]) are the names of each epw
				 file to be created.
	"""
	print("Generating and writing to files")
	if csv is not None:
		csv_output = open(filenames[0], 'w')
		csv_output.write(csv)
		csv_output.close()

	for index, filename in enumerate(filenames[1:]):
		epw_output = open(filename, 'w') #: Open the epw file to be written into.
		try:
			epw_output.write(epws[index]) #: Write the generated string to an epw file.
		except IndexError as e:
			epw_output.write("")
		epw_output.close() #: Close the epw file.

def get_hour_data(hour_data, mlfuncs, file, length, line_list, coords) -> (str, str):
	""" get_hour_data(list):

	This method takes in the list created by main, and
	creates a string following the csv formatting required
	by Energy Plus.

	Args:
		hour_data(list): A list containing all of the information for
				 one hour's worth of data. For any field in the
				 list that is set to None, the returned value in
				 the string corresponding to that None is defined
				 by the missing_values dictionary below.

	Returns:
		(str, str):	A line in the epw file being generated by the main program that
		   		corresponds to the passed list hour_data. for an epq file, If a
				None value is found in hour_data, then a value from the
				missing_values dictionary is assigned to the string based off the
				index of the found None (eg: if a None is found at index 8,
				then "999" will be added to the string). For a csv however, if
				a None is encountered then an empty field is returned.
				NOTE:
					return[0] is the epw line.
					return[1] is the csv line.
	"""

	#: A dictionary to containing the "missing value" values for
	#: each given entry's position in the csv file. For example
	#: a key value pair of -1: "none" would mean that if there is
	#: no data for position -1, then we should enter the word none.
	#: Please note,
	#: 	The values in this dictionary correspond to the format
	#:	found at:
	#: 	https://bigladdersoftware.com/epx/docs/8-8/auxiliary-programs/energyplus-weather-file-epw-data-dictionary.html
	missing_values = {

		#: Note positions 0 to 5 are required positions and are
		#: filled automatically by the program.
		5: "?9?9?9?9E0?9?9?9*9*9?9*9?9*_?9?9*9*9*9*_*9*9*9*9*9", #: References and data flags
		6: "99.9",	 #: Field: Dry bulb temprature.
		7: "99.9",	 #: Field: Dew point temprature.
		8: "999",	 #: Field: Relative humidity.
		9: "101325",	 #: Field: Atmospheric station pressure.
		10: "9999",	 #: Field: Extraterrestrial horizontal radiation.
		11: "9999",	 #: Field: Extraterrestrial direct normal radiation.
		12: "9999",	 #: Field: Infrared radiation intensity.
		13: "9999",	 #: Field: global horizontal radiation.
		14: "9999",	 #: Field: Direct normal radiation.
		15: "9999",	 #: Field: Diffuse horizontal radiation.
		16: "999900",	 #: Field: Global horizontal illuminance.
		17: "999900",	 #: Field: Direct normal illuminance.
		18: "999900",	 #: Field: Diffuse horizontal illuminance.
		19: "9999",	 #: Field: Zenith luminance.
		20: "999",	 #: Field: Wind direction.
		21: "999",	 #: Field: Wind speed.
		22: "5",	 #: Field: Total sky cover.
		23: "5",	 #: Field: Opaque sky cover.
		24: "777.7",	 #: Field: Visibility.
		25: "77777",	 #: Field: Ceiling height.
		26: "9",	 #: Field: Present weather observations.
		27: "", 	 #: Field: Weather codes.
		28: "0",	 #: Field: Precipitable water.
		29: "0.0000",	 #: Field: Aerosol optical depts.
		30: "0",	 #: Field: Snow depth.
		31: "88",	 #: Field: Days since last snowfall.
		32: "999.000",	 #: Field: Albedo.
		33: "999.0",	 #: Field: Liquid precipitation depth
		34: "99.0" 	 #: Field: Liquid precipitation quantity
	}

	#: EnergyPlus enforces strict rounding rules apply to values it processes.
	#: Because more or less data than could be read from sensors that is at
	#: the time of writing this code, a full table will be created to ensure
	#: all data gets correct rounding without neededing for a later user to
	#: pre-process the data.
	rounding_correction = {
		6: 1,
		7: 1,
		8: 0,
		9: 0,
		10: 0,
		11: 0,
		12: 0,
		13: 0,
		14: 0,
		15: 0,
		16: 0,
		17: 0,
		18: 0,
		19: 0,
		20: 0,
		21: 1,
		22: 0,
		23: 0,
		24: 1,
		25: 0,
		26: 0,
		27: 0,
		28: 0,
		29: 4,
		30: 0,
		31: 0,
		32: 3,
		33: 1,
		34: 1

	}

	#: Get the date the this data set.
	cur_date = datetime(int(hour_data[0]), int(hour_data[1]), int(hour_data[2]), int(hour_data[3]) - 1)

	#: Calculate the solar altitude.
	solar_altitude = calc_sol_alt(coords[0], coords[1], -8, cur_date)

	#: Create named variables for the data needed in the various matlab functions.
	#: hour_data[6] contains the dry temprature
	dry_temp = round(hour_data[6], 2) if hour_data[6] is not None else -999
	#: hour_data[8] contains the relative humidity
	relative_humidity = round(hour_data[8], 2) / 100 if hour_data[8] is not None else -999
	#: hour_data[9] contains the pressure
	pressure = round(hour_data[9]) / 100 if hour_data[9] is not None else -999
	#: hour_data[13] contains the global horizontal radiation
	global_horiz_rad = round(hour_data[13], 2) if hour_data[13] is not None else -999

	#: If the dry temprature and relative humidity is not -999 (ie missing)
	#: then get the wet temp and relative humidity.
	if dry_temp != -999 and relative_humidity != -999:
		wet_temp, dew_point_temp = mlfuncs.New_dew_point(float(dry_temp), float(relative_humidity), nargout=2)
	else:
		#: Otherwise set them to -999
		wet_temp, dew_point_temp = -999, -999

	#: Set hour_data[7] to the dew point temprature if its not -999 (ie missing) otherwise None
	hour_data[7] = dew_point_temp if dew_point_temp != -999 else None

	#: Calculate the vapour pressure using the matlab functions
	vapour_pressure = round(mlfuncs.vapor_pressure_MO(float(dry_temp), float(wet_temp), float(pressure)), 3)
	#: Grab the cloud cover value (whichever we can prefering 8) from  cloud_cover.csv
	cloud_cover_converter = get_cloud_data(cur_date, file, length, line_list)
	#L Calculate the cloud cover value.
	cloud_cover = round((int(cloud_cover_converter[0]) / cloud_cover_converter[1]) * 8, 0) if cloud_cover_converter[0] != -999 else -999

	#: Set hour data[22] to the cloud cover if its not -999 (ie missing) otherwise None
	hour_data[22] = round((int(cloud_cover_converter[0]) / cloud_cover_converter[1]) * 10, 0) if cloud_cover_converter[0] != -999 else None

	#: If the dry tempratude, cloud cover, and vapour pressure are not missing,
	#: then calculate the infrared radiation.
	if dry_temp != -999 and cloud_cover != -999 and vapour_pressure != -999:
		infra = mlfuncs.Infrared(float(cloud_cover), float(dry_temp), float(vapour_pressure))
	else:
		#: Otherwise set it to -999 as missing
		infra = -999

	#: If infra is not set as missing, set hour_data[12] as it, otherwise None
	hour_data[12] = infra.real if infra.real != -999 else None

	#: Calculate extraterrestrial horizontal and normal radiation
	extraterr_horiz_rad, extraterr_normal_rad = mlfuncs.Extrasolar_radiation(float(hour_data[0]), float(hour_data[1]), float(hour_data[2]), radians(float(solar_altitude)), nargout=2)

	#: If they are not missing values, set hour_data 10 and 11 to the found values.
	hour_data[10] = extraterr_horiz_rad if extraterr_horiz_rad != -999 else None
	hour_data[11] = extraterr_normal_rad if extraterr_normal_rad != -999 else None

	#: If the solar altitude is greater than 7, calculate the radiations.
	if solar_altitude > 7:
		diffuse_horiz_rad = mlfuncs.Sun_model(float(dry_temp), float(dew_point_temp), float(cloud_cover), float(relative_humidity), float(wet_temp), float(pressure), float(hour_data[0]), float(hour_data[1]), float(hour_data[2]), float(hour_data[3]), float(coords[0]), float(coords[1]), float(coords[2]), radians(float(solar_altitude)))

	#: Otherwise if the solar altitude is between 7 and -5
	#: then there is only diffuse horizontal radiation
	elif solar_altitude > -5:
		diffuse_horiz_rad = [[0, 0, 0, 0, global_horiz_rad]]

	#: Otherwise there is no radiation of these types
	else:
		diffuse_horiz_rad = [[0, 0, 0, 0, 0]]

	#: If the infrared radiation is -999 then the calculated radiations are
	#: not correct so throw them out.
	if infra == -999:
		diffuse_horiz_rad[0][3] = -999
		diffuse_horiz_rad[0][4] = -999

	#: Set hour data 14 and 15 to the different radiations
	hour_data[14] = diffuse_horiz_rad[0][3].real if diffuse_horiz_rad[0][3].real != -999 else None
	hour_data[15] = diffuse_horiz_rad[0][4].real if diffuse_horiz_rad[0][4].real != -999 else None

	#: If diffuse horizontal radiation and infrared radiation are not -999 then
	#: calculate them and set the hour data slots to the calculated values
	if diffuse_horiz_rad[0][4].real != -999 and infra != -999:
		global_horiz_illuminance, direct_horiz_illuminance, diffuse_horiz_illuminance, zenith_luminance = mlfuncs.Illuminance(radians(solar_altitude), global_horiz_rad, diffuse_horiz_rad[0][4].real, dew_point_temp, extraterr_horiz_rad, nargout=4)
		hour_data[16] = global_horiz_illuminance if global_horiz_illuminance != -999 else None
		hour_data[17] = direct_horiz_illuminance if direct_horiz_illuminance != -999 else None
		hour_data[18] = diffuse_horiz_illuminance if diffuse_horiz_illuminance != -999 else None
		hour_data[19] = zenith_luminance if zenith_luminance != -999 else None

	epw_builder = "" #: A string used to build a line of the epw file.
	csv_builder = "" #: A string used to build a line of the csv file.

	#: Loop through the passed data.
	for index, data in enumerate(hour_data):

		#: If this item is None then set it to a missing value
		#: based off of the index of the item.
		if data is None:

			#: Append the value corresponding to a missing value
			#: for that index in the csv.
			epw_builder += missing_values[index]

		#: Otherwise append the data found.
		else:
			#: Indexes before 6 are numbers.
			if index >=6:

				#: Create a string for formatting the string of the data
				#: to have enough decimal places as required.
				z_format = ".{}f".format(rounding_correction[index])

				#: Format the value
				value = format(round(data.real, rounding_correction[index]), z_format)
			else:
				#: Otherwise set value to be a string of the data.
				value = str(data)

			#: Add the value to the epw_builder and if index is not 5, the csv as well
			epw_builder += value
			if index != 5:
				csv_builder += value

		#: If this is not the final item, append a comma.
		if index != 34:
			epw_builder += ","
			csv_builder += ","

	return (epw_builder, csv_builder) #: Return the built string.

def generate_missing_entries(stationid: int, from_date: datetime, to_date:datetime = None, prepend_newl: bool=False) -> [str, str]:
	""" generate_missing_entries(int, datetime, dattime, bool)

	Due to EnergyPlus's strict rules on data for each day and each
	month within a peroid being complete, empty data must be filled in
	for missing days.

	This method will generate all the data for the missing days for each
	day and hour that is missing.

	Args:
		from_date(datetime):	A datetime object representing the last day/time
					for which data is available.

		to_date(datetime): 	A datetime object representing the next day/time that
					data is availiable. If None it is assumed that there
					is no more available data.

		prepend_newl(bool):	If true, a newline character will be prepended to the
					line.

	Returns:
		[str, str]:	Similar to get_hour_data, the first string represents
				the line(s) of missing data for the epw file, and the
				second represents the line(s) of missing date for the
				csv file.

	"""

	#: A dictionary to containing the "missing value" values for
	#: each given entry's position in the csv file. For example
	#: a key value pair of -1: "none" would mean that if there is
	#: no data for position -1, then we should enter the word none.
	#: Please note,
	#: 	The values in this dictionary correspond to the format
	#:	found at:
	#: 	https://bigladdersoftware.com/epx/docs/8-8/auxiliary-programs/energyplus-weather-file-epw-data-dictionary.html
	missing_values = {

		#: Note positions 0 to 5 are required positions and are
		#: filled automatically by the program.
		6: "99.9",	 #: Field: Dry bulb temprature.
		7: "99.9",	 #: Field: Dew point temprature.
		8: "999",	 #: Field: Relative humidity.
		9: "101325",	 #: Field: Atmospheric station pressure.
		10: "9999",	 #: Field: Extraterrestrial horizontal radiation.
		11: "9999",	 #: Field: Extraterrestrial direct normal radiation.
		12: "9999",	 #: Field: Infrared radiation intensity.
		13: "9999",	 #: Field: global horizontal radiation.
		14: "9999",	 #: Field: Direct normal radiation.
		15: "9999",	 #: Field: Diffuse horizontal radiation.
		16: "999900",	 #: Field: Global horizontal illuminance.
		17: "999900",	 #: Field: Direct normal illuminance.
		18: "999900",	 #: Field: Diffuse horizontal illuminance.
		19: "9999",	 #: Field: Zenith luminance.
		20: "999",	 #: Field: Wind direction.
		21: "999",	 #: Field: Wind speed.
		22: "5",	 #: Field: Total sky cover.
		23: "5",	 #: Field: Opaque sky cover.
		24: "777.7",	 #: Field: Visibility.
		25: "77777",	 #: Field: Ceiling height.
		26: "9",	 #: Field: Present weather observations.
		27: "", 	 #: Field: Weather codes.
		28: "0",	 #: Field: Precipitable water.
		29: "0.0000",	 #: Field: Aerosol optical depts.
		30: "0",	 #: Field: Snow depth.
		31: "88",	 #: Field: Days since last snowfall.
		32: "999.000",	 #: Field: Albedo.
		33: "999.0",	 #: Field: Liquid precipitation depth
		34: "99.0" 	 #: Field: Liquid precipitation quantity
	}

	#: If to_date is None, then we presume we must generate data to the end
	#: of the current month.
	if to_date is None:
		if from_date.month != 12:
			to_date = datetime(from_date.year, from_date.month + 1, 1, 0)
		else:
			to_date = datetime(from_date.year + 1, 1, 1, 0)

	#: Get the day of the last known existing data.
	cur_day = from_date.day
	cur_month = from_date.month
	cur_year = from_date.year

	#: Increment the hour by one as this is the first hour we need to
	#: generate missing data for.
	cur_hour = from_date.hour + 1

	#: if prepend_newl is True, then start both strings with a newline character,
	#: otherwise start both as empty strings.
	epw_string = "\n" if prepend_newl else ""
	csv_string = "\n" if prepend_newl else ""

	#: Because all missing data rows look the same after date information,
	#: we can generate this once, and just append it to each row.
	epw_string_end = ""
	csv_string_end = ""
	for index in range(6, 35):
		epw_string_end += missing_values[index] + ','
		csv_string_end += ','

	#: Generate the missing data.

	missing_rows = int((to_date - from_date).total_seconds() / 3600)

	print("Generating " + str(missing_rows)  + " row(s) of missing data for " + str(from_date))
	while from_date != to_date:

		#: Prepend the date information to each missing row of data.
		epw_string += "{},{},{},{},0,,".format(cur_year, cur_month, cur_day, cur_hour)
		csv_string += "{},{},{},{},0,,".format(cur_year, cur_month, cur_day, cur_hour)

		#: Append the empty row data.
		epw_string += epw_string_end
		csv_string += csv_string_end

		epw_string += '\n' #: End of an hour means end of a row.
		csv_string += '\n{},'.format(stationid)

		from_date += timedelta(hours=1)
		cur_year = from_date.year
		cur_month = from_date.month
		cur_day = from_date.day
		cur_hour = from_date.hour + 1

	return [epw_string, csv_string] #: Return the missing data.

def get_cloud_data(date: datetime, csvfile, filelength, line_list):
	"""	get_cloud_data(datetime, FILE, int, list)

		Reads a specific csv file, and reads cloud data from it.

		Args:
			date(datetime): The time of the data currently being looked at.
			csvfile(FILE): The csv file being read.
			filelength(int): The number of lines in the file.
			line_list(list): A list of numbers corresponding indexes to the character
					 of the line of that index. IE: line_list[3] has the number
					 of the first character of the third line - 1
	"""

	#: There is no data from January 2008 and earlier, so reutrn -999 for an unknown value
	if date.year <= 2008:
		if date.year == 2008 and date.month == 1:
			return [-999, 0]
		elif date.year < 2008:
			return [-999, 0]

	#: Set the cursor to the first non-header line in the file.
	csvfile.seek(line_list[1] + 1)
	#: Read the line, removing the "" signs, and splitting over commas.
	csv_row = csvfile.readline().replace("\"", '').split(",")
	#: Get the date out of this row, as this is the latest data value
	max_date = datetime.strptime(csv_row[0][:-4], "%Y-%m-%d %H:%M:%S")
	#: If the found final date is less that the current day, return unknown values
	if max_date < date:
		return [-999, 0]

	#: Get both the PST and PDT time stamps for the passed date
	timestamp = date.strftime('%Y-%m-%d %H:00:00 PST')
	date += timedelta(hours=1)
	alt_timestamp = date.strftime('%Y-%m-%d %H:00:00 PDT')

	#: Find the amount of time between the top of the file and
	#: the passed date's entry
	tdiff = max_date - date
	#: Calculate the number of rows between the top row and the passed date.
	hours_since_first_data = int((tdiff.days * 24) + (tdiff.seconds / 3600)) + 2

	#: find the specific chacter that line starts on.
	absolute_position = line_list[hours_since_first_data]
	#: Go to that position (plus one to get onto the line, instead of being on the newline.)
	csvfile.seek(absolute_position + 1)
	#: Read the line of data.
	row = csvfile.readline().replace("\"", '').split(",")

	#: If the time timestamp is a PST time stamp:
	if "PST" in row[0]:
		#: Check that row[0] equals the timestamp (as it should)
		if row[0] == timestamp:
			#: If row[-2] != '', then there is data for the 0 to 10 cloud cover value
			#: so return that value and 10
			if row[-2] != '':
				return [row[-2], 10]
			#: If row[-3] != '', then there is data for the 0 to 8 cloud cover value
			#: so return that value and 8
			elif row[-3] != '':
				return [row[-3], 8]
			#: If row[-4] != '', then there is data for the 0 to 4 cloud cover value
			#: so return that value and 4
			elif row[-4] != '':
				return [row[-4], 4]
			#: Otherwise there is no cloud cover data, then return that theres a missing
			#: value
			else:
				return [-999, 0]
	elif "PDT" in row[0]:
		if row[0] == alt_timestamp:
			#: If row[-2] != '', then there is data for the 0 to 10 cloud cover value
			#: so return that value and 10
			if row[-2] != '':
				return [row[-2], 10]
			#: If row[-3] != '', then there is data for the 0 to 8 cloud cover value
			#: so return that value and 8
			elif row[-3] != '':
				return [row[-3], 8]
			#: If row[-4] != '', then there is data for the 0 to 4 cloud cover value
			#: so return that value and 4
			elif row[-4] != '':
				return [row[-4], 4]
			#: Otherwise there is no cloud cover data, then return that theres a missing
			#: value
			else:
				return [-999, 0]

	#: This means that the timestamps dont match, so a row near the printed timestamps
	#: is missing. Raise an exeption as this means all other data will be incorrect.
	print("Failed to find cloud data for {}, or {}.".format(timestamp, alt_timestamp))
	raise Exception

if __name__ == "__main__":

	#: Create base argument parser.
	parser = argparse.ArgumentParser(description="Command line utility to pull weather data from an sql"\
						     " database.")
	#: Add the station argument
	parser.add_argument('station', type=int, help='The id of the station to get data from.')

	parser.add_argument('-p', '--password', type=str,
			    help='The password for the MySQLDatabase. If none is provided, it will be read from mysqlpassword.txt')

	#: Create a group so user can select between by_list and by_range.
	by_type = parser.add_mutually_exclusive_group(required = True)

	#: Add the by_range argument
	by_type.add_argument('-r', '--by_range', action='store_true',
				 help='Default: used to select ranges of dates to get data from.')

	#: Add the by_list argument.
	by_type.add_argument('-l', '--by_list', action='store_true',
				help='Used to select specific months within each year to get data from.')

	#: Add the from_year argument for by_range
	parser.add_argument('-fy','--from_year', type=int, help='The year to start gathering data from.')

	#: Add the from_month argument for by_range
	parser.add_argument('-fm', '--from_month', type=int,
				help='The number representing the month to start gathering data from.')

	#: Add the to_year argument for by_range
	parser.add_argument('-ty', '--to_year', type=int,
				  help='The year to stop gathering data from.')

	#: Add the to_month argument for by_range
	parser.add_argument('-tm', '--to_month', type=int,
				  help='The number representing the month to stop gathering data from.')

	#: Add the years argument for by_list
	parser.add_argument('-y', '--years', type=int, nargs='+', help='The years to collect data from.')

	#: Add the months argument for by_list
	parser.add_argument('-m', '--months', type=int, nargs='+', help='The months to collect data from.')

	#: Parse the arguments.
	args = parser.parse_args()

	if args.password is None:
		print("No password passed, reading from mysqlpassword.txt")
		with open("mysqlpassword.txt") as passfile:
			args.password = passfile.readline()

	#: If we are pulling data by a range
	if args.by_range:

		#: Check that all required arguments exist.
		#: If one doesn't, show the help dialog, and
		#: tell them which one is missing.
		if args.from_year is None:
			parser.print_usage()
			print("CreateHourlyDataFrames.py: error: the argument "\
			      "-fy/--from_year is required with the -r/--by_range switch.")
			exit()
		if args.to_year is None:
			parser.print_usage()
			print("CreateHourlyDataFrames.py: error: the argument "\
			      "-ty/--to_year is required with the -r/--by_range switch.")
			exit()
		if args.from_month is None:
			parser.print_usage()
			print("CreateHourlyDataFrames.py: error: the argument "\
			      "-fm/--from_month is required with the -r/--by_range switch.")
			exit()
		if args.to_month is None:
			parser.print_usage()
			print("CreateHourlyDataFrames.py: error: the argument "\
			      "-tm/--to_month is required with the -r/--by_range switch.")
			exit()

		#: Warn users about unused values that they've passed.
		if args.months is not None:
			print("Warning: Value passed for --months will be ignored.")
		if args.years is not None:
			print("Warning: Value passed for --years will be ignored.")

		#: Generate the from date.
		from_date = datetime(args.from_year, args.from_month, 1, 0)
		#: Generate the to date.
		to_date = datetime(args.to_year, args.to_month, 1, 0)

		#: Get the data.
		strings = get_hourly_weather_data(args.password, args.station, from_date=from_date, to_date=to_date)

	if args.by_list:

		#: Check that all required arguments exist.
		#: If one doesn't, show the help dialog, and
		#: tell them which one is missing.
		if args.months is None:
			parser.print_usage()
			print("CreateHourlyDataFrames.py: error: the argument "\
			      "-m/--month is required with the -l/--by_list switch.")
			exit()
		if args.years is None:
			parser.print_usage()
			print("CreateHourlyDataFrames.py: error: the argument "\
			      "-y/--year is required with the -l/--by_list switch.")
			exit()

		#: Warn users about unused values that they've passed.
		if args.from_year is not None:
			print("Warning: Value passed for --from_year will be ignored.")
		if args.to_year is not None:
			print("Warning: Value passed for --to_year will be ignored.")
		if args.from_month is not None:
			print("Warning: Value passed for --from_month will be ignored.")
		if args.to_month is not None:
			print("Warning: Value passed for --to_month will be ignored")

		#: Turn the passed values into 2 digit strings representing each month.
		months = []
		for month in args.months:
			months.append(str(month).zfill(2))

		#: Get the data.
		strings = get_hourly_weather_data(args.password, args.station, months=months, years=args.years)

	print(strings[0])
	print(strings[1])
	print(strings[2])
	write_data_to_files(strings[0], strings[2], strings[1])

