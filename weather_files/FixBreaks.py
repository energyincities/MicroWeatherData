import os
import csv
import datetime

def main():

	files = []

	for file in [os.path.abspath(f) for f in os.listdir() if f[-4:] == ".epw"]:
		print(file)

		with open(file, 'r') as e:
			data = e.readlines()
			if len(data) == 0:
				continue

		data[7] = "DATA PERIODS,1,1,Data,Sunday, 1/ 1,12/31\n"
		yr = int(data[8][:4])

		timestamps = []

		for line in data[8:]:
			if len(line) < 10:
				data.remove(line)
				continue

			r = line.split(",")
			cur_date = datetime.datetime(int(r[0]), int(r[1]), int(r[2]), int(r[3]) - 1)
			timestamps.append(cur_date)

		marks = []

		for index, line in enumerate(data[8:]):

			t_row = line.split(",")
			cur_date = datetime.datetime(int(t_row[0]), int(t_row[1]), int(t_row[2]), int(t_row[3]) - 1)

			if timestamps.count(cur_date) > 1:
				p_row = data[index + 7].split(",")
				p_date = datetime.datetime(int(p_row[0]), int(p_row[1]), int(p_row[2]), int(p_row[3]) - 1)
				if cur_date - datetime.timedelta(hours=1) != p_date:
					marks.append(index+8)

		for mark in marks[::-1]:
			if mark == len(data) - 1:
				data = data[:-1]
			elif mark == 0:
				data = data[1:]
			else:
				print("Removing: ", data[mark])
				data= data[:mark] + data[mark+1:]

		marks = []

		date = datetime.datetime(yr, 1, 1, 0)
		for index, line in enumerate(data[8:]):

			t_row = line.split(",")
			cur_date = datetime.datetime(int(t_row[0]), int(t_row[1]), int(t_row[2]), int(t_row[3]) - 1)

			if len(t_row) > 35:
				data[index+8] = ",".join(t_row[:34]) + ",99.0,\n"

			while cur_date > date:

				marks.append([date, index])
				date += datetime.timedelta(hours=1)

			date += datetime.timedelta(hours=1)

			if ",,99.9,999,101325" in line:
				data[index+8] = data[index+8].replace(",,99.9,999", ",,99.9,99.9,999")

		marks = sorted(marks, key=lambda x: x[1])

		for missing_set in marks[::-1]:
			year = str(missing_set[0].year) + ","
			month = str(missing_set[0].month) + ","
			day = str(missing_set[0].day) + ","
			hour = str(missing_set[0].hour + 1) + ","
			print("readding: ", year, month, day, hour)
			missing_string = year + month + day + hour + "0,,99.9,99.9,999,101325,9999,9999,9999,9999,9999,9999,999900,999900,999900,9999,999,999,5,5,777.7,77777,9,,0,0.0000,0,88,999.000,999.0,99.0,\n"
			data.insert(missing_set[1] + 8, missing_string)

		data.append(data[-1].replace("23,", "24,"))

		filename = "weather_data_for_Jan_{}_to_Jan_{}".format(yr, yr+1)
		filename += file[file.index('_at_'):]

		files.append([filename, data])


	for file in [os.path.abspath(f) for f in os.listdir() if f[-4:] == ".epw"]:
		os.remove(file)

	for d in files:
		with open(d[0], 'w+') as e:
			for line in d[1]:
				e.write(line)

if __name__ == "__main__":
	main()
